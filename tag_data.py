#!/usr/bin/python

__author__ = "Mike Oldham (tarous@gmail.com)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2005/11/29 00:12:26 $"
__copyright__ = "Copyright (c) 2005-2006 Mike Oldham"
__license__ = "CC"

import os,getopt, sys, struct
from tags.AMSO import AMSO 
from tags.ANIM import ANIM 
from tags.ARTI import ARTI 
from tags.COLL import COLL
from tags.CONN import CONN
from tags.CREF import CREF
from tags.DMAP import DMAP
from tags.DTEX import DTEX
from tags.GEOM import GEOM
from tags.LIGH import LIGH
from tags.LPGR import LPGR
from tags.MEDI import MEDI 
from tags.MEEF import MEEF 
from tags.MESH import MESH 
from tags.MODE import MODE
from tags.MONS import MONS
from tags.OBJE import OBJE
from tags.PART import PART
from tags.PHYS import PHYS 
from tags.PREL import PREL
from tags.PRGR import PRGR
from tags.PROJ import PROJ
from tags.SCEN import SCEN
from tags.SOUN import SOUN
from tags.STLI import STLI
from tags.TEXT import TEXT
from tags.UNIT import UNIT
from tags.GENERIC import GENERIC
import tag_header
        
class localInfo:
    def __init__(self, m2Path=False):
        if m2Path:
            self.m2Path=m2Path
            self.TAG_FILE_ENTRY_POINT_COUNT=112
            self.localPath=m2Path + "local/"
            self.chaosLocalPath=m2Path + "chaos.locals/"
            self.tagsPath=m2Path + "tags/"
            self.pluginsPath=m2Path + "plugins/"
            self.pluginName=""
            self.LOCALTYPE, self.PLUGINSTYPE, self.PATCHTYPE, self.TAGSTYPE=0,1,2,3
            self.pluginList = []
            self.didError=False
        
    def loadTags(self, printFunction, pluginName=""):
        self.printFunction=printFunction
        self.tagsDictionary={}
        self.pluginList = []
        rootList= os.listdir(self.m2Path)
        if "tags" in rootList:
            bigTagList = self.ReadTags(self.tagsPath, self.TAGSTYPE)
            self.addTagsToDictionary(self.tagsDictionary, bigTagList)
        if "plugins" in rootList:  
            bigTagList = self.ReadTags(self.pluginsPath, self.PATCHTYPE)
            self.addTagsToDictionary(self.tagsDictionary, bigTagList)
            if pluginName != "":
                self.pluginName = pluginName
                bigTagList = self.ReadTags(self.pluginsPath, self.PLUGINSTYPE, True)
                self.addTagsToDictionary(self.tagsDictionary, bigTagList)
        if "local" in rootList:
            self.printFunction("Loading local files...")
            bigTagList = self.ReadTags(self.localPath, self.LOCALTYPE)
            self.addTagsToDictionary(self.tagsDictionary, bigTagList)
        
    
    def addTagsToDictionary(self, theDictionary, theTagList):
        for thisTag in theTagList:
            tagType=thisTag.tagHeader.getGroup_Tag()
            if tagType not in theDictionary:
                theDictionary[tagType]=[]
            self.tryToAppendTagList(theDictionary[tagType],thisTag)
    
    def tryToAppendTagList(self, theArray, theTag):
        shouldAppendTag=True
        for i in range(len(theArray)):
            tmpTag=theArray[i]
            if tmpTag.tagHeader.getID()==theTag.tagHeader.getID():
                shouldAppendTag=False
                theArray[i]=theTag
                break
        if shouldAppendTag:
            theArray.append(theTag)
        
    def getTagList(self, groupTag, localOnly):
        if groupTag in self.tagsDictionary:
            if localOnly:
                tmpList=[]
                for dObject in self.tagsDictionary[groupTag]:
                    if dObject.isLocal:
                        tmpList.append(dObject)
                return tmpList;
            else:
                return self.tagsDictionary[groupTag]
        else:
            return []
            
    def getUnknownTag(self, tFile, tPath, tSpot,isLocal):
        tmpHeader = tag_header.Tag_Header(tFile,tSpot)
        return self.getTag(tFile, tPath,  tSpot, isLocal, tmpHeader.getGroup_Tag())
        
    def getTag(self, tFile, tPath, tSpot,isLocal, tagType):
        if tagType=='amso':
            return AMSO(tFile, tSpot, isLocal, tPath)
        elif tagType=='anim':
            return ANIM(tFile, tSpot, isLocal, tPath)
        elif tagType=='arti':
            return ARTI(tFile, tSpot, isLocal, tPath)
        elif tagType=='.256':
            return COLL(tFile, tSpot, isLocal, tPath)
        elif tagType=='conn':
            return CONN(tFile, tSpot, isLocal, tPath)
        elif tagType=='core':
            return CREF(tFile, tSpot, isLocal, tPath)
        elif tagType=='dmap':
            return DMAP(tFile, tSpot, isLocal, tPath)
        elif tagType=='dtex':
            return DTEX(tFile, tSpot, isLocal, tPath)
        elif tagType=='geom':
            return GEOM(tFile, tSpot, isLocal, tPath)
        elif tagType=='ligh':
            return LIGH(tFile, tSpot, isLocal, tPath)
        elif tagType=='lpgr':
            return LPGR(tFile, tSpot, isLocal, tPath)
        elif tagType=='medi':
            return MEDI(tFile, tSpot, isLocal, tPath)
        elif tagType=='meef':
            return MEEF(tFile, tSpot, isLocal, tPath)
        elif tagType=='mesh':
            return MESH(tFile, tSpot, isLocal, tPath)
        elif tagType=='mode':
            return MODE(tFile, tSpot, isLocal, tPath)
        elif tagType=='mons':
            return MONS(tFile, tSpot, isLocal, tPath)
        elif tagType=='obje':
            return OBJE(tFile, tSpot, isLocal, tPath)
        elif tagType=='part':
            return PART(tFile, tSpot, isLocal, tPath)
        elif tagType=='phys':
            return PHYS(tFile, tSpot, isLocal, tPath)
        elif tagType=='prel':
            return PREL(tFile, tSpot, isLocal, tPath)
        elif tagType=='prgr':
            return PRGR(tFile, tSpot, isLocal, tPath)
        elif tagType=='proj':
            return PROJ(tFile, tSpot, isLocal, tPath)
        elif tagType=='scen':
            return SCEN(tFile, tSpot, isLocal, tPath)
        elif tagType=='soun':
            return SOUN(tFile, tSpot, isLocal, tPath)
        elif tagType=='stli':
            return STLI(tFile, tSpot, isLocal, tPath)
        elif tagType=='text':
            return TEXT(tFile, tSpot, isLocal, tPath)
        elif tagType=='unit':
            return UNIT(tFile, tSpot, isLocal, tPath)
        else:    
            return GENERIC(tFile, tSpot, isLocal, tPath)
            #return False
            
    def readTagData(self,fPath, mySpot, isLocal, groupTag, tagCList):
        tagList=[]
        doContinue=True
        while doContinue:
            if os.path.isfile(fPath):
                tagFile = file(fPath, "rb")
                tagFile.seek(mySpot)
                tmpData=tagFile.read(64)
                if len(tmpData)>=64:
                    myTag = self.getTag(tagFile, fPath, mySpot, isLocal, groupTag)
                    if (myTag.tagHeader.getGroup_Tag()==groupTag):
                        if (myTag.tagHeader.getSubgroup_Tag() in tagCList)==False:
                            tagCList.append(myTag.tagHeader.getSubgroup_Tag())
                            tagList.append(myTag)
                        mySpot=mySpot+64
                    else:
                        doContinue=False
                else:
                    doContinue=False
                tagFile.close()
        return tagList
        
    def ReadTags(self, thePath, tagType, isPlugin=False):
        tmpList=[]
        for dFile in os.listdir(thePath):   
            if dFile != ".DS_Store":  
                fPath=thePath + dFile
                if os.path.isfile(fPath):
                    if (not isPlugin) or (dFile == self.pluginName):  
                        tagFile = file(fPath, "rb")
                        if tagType==self.LOCALTYPE:
                            tmpData=tagFile.read(64)
                            if len(tmpData)>=64: 
                                tagFile.seek(0)  
                                myTag = self.getUnknownTag(tagFile, fPath, 0, True)
                                if myTag!=False:
                                    myTag.tagHeader.setTagType(tagType)
                                    tmpList.append(myTag)
                        else:
                            tmpData=tagFile.read(128)
                            if len(tmpData)>=128:  
                                tagFile.seek(0)
                                tagHeader=tag_header.tag_file_header(tagFile)
                                if (not isPlugin) and (tagHeader.type==self.PLUGINSTYPE):
                                    self.pluginList.append(dFile)
                                if (tagHeader.type==tagType): 
                                    self.printFunction("Loading " + tagHeader.name + "...")
                                    tagFile.seek((self.TAG_FILE_ENTRY_POINT_COUNT * tagHeader.entry_point_count)+128)
                                    dOffset=tagFile.tell()
                                    for i in range(tagHeader.tag_count):    
                                        myTag = self.getUnknownTag(tagFile, fPath, dOffset, False)
                                        if myTag!=False:
                                            myTag.tagHeader.setTagType(tagType)
                                            tmpList.append(myTag)
                                        dOffset=dOffset+64
                elif os.path.isdir(fPath):
                    tmpList.extend(self.ReadTags(fPath + "/", tagType, isPlugin))
        return tmpList
                                
                                
