from distutils.core import setup
import py2app

py2app_options = dict(
    iconfile='Chaos.icns',
    plist='Info.plist',
)

setup(
    app=['TagController.py'],
    data_files=['English.lproj', 'mythTags.py', 'tag_data.py', 'Chaos.icns', 'tags'],
    options=dict(py2app=py2app_options,)
)
