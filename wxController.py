#!/usr/bin/pythonw

__author__ = "Mike Oldham (tarous@gmail.com)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2005/11/29 00:12:26 $"
__copyright__ = "Copyright (c) 2011 Mike Oldham"
__license__ = "CC"

import wx, os
from mythTags import mythTags, localInfo

########################################################################################################
# This is my wxPython Interface. I know it ain't pretty, but it does work, and it works cross platform.#
########################################################################################################

ID_REFRESH=102
ID_MONS_BUTTON, ID_PROJ_BUTTON, ID_OBJE_BUTTON, ID_UNIT_BUTTON=115, 116, 117, 118
ID_MONS_COMBO, ID_PROJ_COMBO, ID_OBJE_COMBO, ID_UNIT_COMBO=215, 216, 217, 218
ID_MONS_CHECKBOX, ID_PROJ_CHECKBOX, ID_OBJE_CHECKBOX, ID_UNIT_CHECKBOX=315, 316, 317, 318
PATH_TO_TAGS="./"
#PATH_TO_TAGS="dist/" #When testing on the mac uncomment this line
class MainWindow(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self,parent,id, title):
        wx.Frame.__init__(self,parent,wx.ID_ANY,title,size=(640,640),style=wx.DEFAULT_FRAME_STYLE|wx.NO_FULL_REPAINT_ON_RESIZE)
        self.CreateStatusBar() # A Statusbar in the bottom of the window
        # Setting up the menu.
        filemenu= wx.Menu()
        #filemenu.Append(ID_REFRESH, "&Refresh", " RefreshRefresh tag list")
        aboutMenu = filemenu.Append(wx.ID_ABOUT, "&About"," Information about this program")
        filemenu.AppendSeparator()
        exitMenu = filemenu.Append(wx.ID_EXIT, "E&xit"," Exit this program")
        # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu,"&File") # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutMenu)
        self.Bind(wx.EVT_MENU, self.OnExit, exitMenu)
        
        ########PLUGIN###########
        pluginPanel=wx.Panel(self)
        wx.StaticText(pluginPanel, -1, "Select a Plugin:", wx.Point(10,9))
        self.pluginComboBox=wx.ComboBox(pluginPanel, -1, 'None', wx.Point(85,05), wx.Size(250,30), ['None'], wx.CB_READONLY)
        self.Bind(wx.EVT_COMBOBOX, self.choosePlugin, self.pluginComboBox)
        
        ##########TAB VIEW#########
        self.sizer=wx.BoxSizer(wx.VERTICAL)
        self.notebook=wx.Notebook(self, -1)
  
        ##########MONSTER##########
        
        self.monsPanel=wx.Panel(self.notebook)
        self.notebook.AddPage(self.monsPanel, "Monster")
        self.monsList = ['Find Monsters not linked to a Unit', 'List Units linked to Monster']
        self.monsterRadio = wx.RadioBox(self.monsPanel, -1, "", wx.Point(5,10), wx.DefaultSize, self.monsList, 1, wx.RA_SPECIFY_COLS)
        self.Bind(wx.EVT_RADIOBOX, self.monsRadioChange, self.monsterRadio)
        self.monsCheckbox=wx.CheckBox(self.monsPanel, ID_MONS_CHECKBOX, "local only", wx.Point(320,35), wx.Size(95,20))
        self.okButton=wx.Button(self.monsPanel, ID_MONS_BUTTON, "Go", wx.Point(320,65), wx.Size(95,20))
        self.Bind(wx.EVT_BUTTON, self.monsGo, self.okButton)
        self.monsList=['']
        self.monsComboBox=wx.ComboBox(self.monsPanel, ID_MONS_COMBO, self.monsList[0], wx.Point(280,05), wx.Size(144,30),self.monsList,wx.CB_READONLY)
        self.monsComboBox.Show(False)
        

        
        
        ##########PROJECTILE##########
        
        self.projPanel=wx.Panel(self.notebook)
        self.notebook.AddPage(self.projPanel, "Projectile")
        self.okButton=wx.Button(self.projPanel, ID_PROJ_BUTTON, "Go", wx.Point(320,65), wx.Size(95,20))
        self.projCheckbox=wx.CheckBox(self.projPanel, ID_PROJ_CHECKBOX, "local only", wx.Point(320,35), wx.Size(95,20))
        self.projList=['Find Projectiles by Damage Type', 'Find Projectiles by Inertia Type', 'Find Projectiles by Class Type', 'List Projectiles in PRGR\'s', 'List Unused Projectiles', 'List Dormant Info for Projectiles']
        self.projectileRadio = wx.RadioBox(self.projPanel, -1, "", wx.Point(5,10), wx.DefaultSize, self.projList, 1, wx.RA_SPECIFY_COLS)
        self.Bind(wx.EVT_RADIOBOX, self.projRadioChange, self.projectileRadio)
        self.PDList=["None", "Explosive", "Magic", "Holding", "Healing", "Kenetic", "Slashing", "Stoning", "Slash Claws", "Explosive Electricity", "Fire", "Gas", "Charm"]
        self.InertiaList=["None", "Unwieldy", "Small", "Large"]
        self.ClassList=["None", "Unknown", "Head", "Arm", "Leg", "Torso", "Bloody Chunk", "Lethal Object", "Nonlethal Object", "Explosive Object", "Potentially Explosive Object", "Item"]
        self.projComboBox=wx.ComboBox(self.projPanel, ID_PROJ_COMBO, self.PDList[0], wx.Point(280,05), wx.Size(144,30),self.PDList,wx.CB_READONLY)
        wx.EVT_BUTTON(self, ID_PROJ_BUTTON, self.projGo)
        
        
        ##########OBJECT##########
        
        self.objePanel=wx.Panel(self.notebook)
        self.notebook.AddPage(self.objePanel, "Duplicate/Dead")
        self.okButton=wx.Button(self.objePanel, ID_OBJE_BUTTON, "Go", wx.Point(320,65), wx.Size(95,20))
        self.objeCheckbox=wx.CheckBox(self.objePanel, ID_OBJE_CHECKBOX, "local only", wx.Point(320,35), wx.Size(95,20))
        wx.EVT_BUTTON(self, ID_OBJE_BUTTON, self.objeGo)
        self.objeList=['Find Duplicate Objects', 'Find Dead Tags']
        self.objeRadio = wx.RadioBox(self.objePanel, -1, "", wx.Point(5,10), wx.DefaultSize, self.objeList, 1, wx.RA_SPECIFY_COLS)
        self.Bind(wx.EVT_RADIOBOX, self.objeRadioChange, self.objeRadio)
        
        
        ##########Extraction##########
        
        self.extractionPanel=wx.Panel(self.notebook)
        self.notebook.AddPage(self.extractionPanel, "Extraction")
        self.okButton=wx.Button(self.extractionPanel, ID_UNIT_BUTTON, "Go", wx.Point(320,65), wx.Size(95,20))
        wx.EVT_BUTTON(self, ID_UNIT_BUTTON, self.unitGo)
        self.extractList=['Extract Unit', 'Extract Artifact', 'Extract Scenery', 'Extract String Lists', 'Extract Texts', 'Extract Mesh']
        self.extractionRadio = wx.RadioBox(self.extractionPanel, -1, "", wx.Point(5,10), wx.DefaultSize, self.extractList, 1, wx.RA_SPECIFY_COLS)
        self.Bind(wx.EVT_RADIOBOX, self.extractionRadioChange, self.extractionRadio)
        self.extractionCheckbox=wx.CheckBox(self.extractionPanel, -1, "local only", wx.Point(320,35), wx.Size(95,20))
        self.extractList=['']
        self.extractComboBox=wx.ComboBox(self.extractionPanel, ID_UNIT_COMBO, self.monsList[0], wx.Point(280,05), wx.Size(144,30),self.monsList,wx.CB_READONLY)
        
        
        
        self.control = wx.TextCtrl(self,1,style=wx.TE_MULTILINE)
        
        self.sizer.Add(pluginPanel, 0,wx.EXPAND) 
        self.sizer.Add(self.notebook,0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND)
        self.sizer.Add(self.control,1,wx.EXPAND)
        self.SetSizer(self.sizer)
        self.shouldAppend = False
        
        self.Show(True)

    def refreshTags(self, printFunction, pluginName=""):
        self.mTags=mythTags()
        self.newLocal.loadTags(printFunction, pluginName)
        
        lastValue = self.pluginComboBox.GetValue()
        self.pluginComboBox.Clear()
        self.pluginComboBox.Append("None")
        self.pluginComboBox.AppendItems(self.newLocal.pluginList)
        self.pluginComboBox.SetValue(lastValue)
        
        tmpList=self.newLocal.getTagList('mons', False)
        tmpList.sort()
        self.monsList=['All']
        for mons in tmpList:
            self.monsList.append(mons.getmName())
        self.monsComboBox.Clear()
        for dString in self.monsList:
            self.monsComboBox.Append(dString)
        self.monsComboBox.SetSelection(0)
        
        if self.extractionRadio.GetSelection()==0: 
            tagType="unit"
        elif self.extractionRadio.GetSelection()==1:
            tagType="arti"
        elif self.extractionRadio.GetSelection()==2:
            tagType="scen"
        elif self.extractionRadio.GetSelection()==5:
            tagType="mesh"
        tmpList=self.newLocal.getTagList(tagType, False)
        self.extractList=[]
        for eTag in tmpList:
            self.extractList.append(eTag.tagHeader.name)
        self.extractList.sort()
        self.extractComboBox.Clear()    
        for dString in self.extractList:
            self.extractComboBox.Append(dString)
        self.extractComboBox.SetSelection(0)
        
    def printToDialog(self, theString):
        print theString
    
    def printToOutput(self, theString):
        if self.shouldAppend:
            self.control.AppendText(theString+"\n")
        else:
            self.control.Clear()
            self.control.SetValue(theString+"\n")
        
    
    def OnExit(self,e):
        self.Destroy()
    def OnAbout(self,e):
        d= wx.MessageDialog(self, "Chaos is a mapmakers diagnostics tool.\n" + "Copyright 2005-2011 Mike Oldham", "About Chaos", wx.OK)
                            # Create a message dialog box
        d.ShowModal() # Shows it
        d.Destroy() # finally destroy it when finished.
        
        
    def choosePlugin(self, e):
        self.printToOutput("Updating tag list please wait...")
        self.shouldAppend=True
        pluginName = ""
        if self.pluginComboBox.GetSelection()!=0:
           pluginName = self.pluginComboBox.GetValue()
        self.refreshTags(self.printToOutput, pluginName)
        self.printToOutput("Tag list updated.")
        self.shouldAppend=False
        
    def monsRadioChange(self, e):
        if self.monsterRadio.GetSelection()==0: 
            self.monsComboBox.Show(False)
        elif self.monsterRadio.GetSelection()==1:
            self.monsComboBox.Show(True)
            
    def extractionRadioChange(self, e):
        tagIndex = self.extractionRadio.GetSelection()
        if tagIndex==0: 
            tagType="unit"
        elif tagIndex==1:
            tagType="arti"
        elif tagIndex==2:
            tagType="scen"
        elif tagIndex==5:
            tagType="mesh"
        if tagIndex < 3 or tagIndex == 5:
            tmpList=self.newLocal.getTagList(tagType, False)
            self.extractList=[]
            for eTag in tmpList:
                self.extractList.append(eTag.getTagName())
            if len(self.extractList)==0:
                self.extractList.append("Nothing to Extract")
            else:
                self.extractList.sort()
            self.extractComboBox.Clear()
            for dString in self.extractList:
                self.extractComboBox.Append(dString)
            self.extractComboBox.SetSelection(0)
            self.extractComboBox.Show(True)
        else:
            self.extractComboBox.Show(False)
        
        
        
    def objeRadioChange(self, e):
        if self.objeRadio.GetSelection()==0: #Find Duplicate Objects
            self.objeCheckbox.Show(True)
        elif self.objeRadio.GetSelection()==1: #Find Dead Tags
            self.objeCheckbox.Show(False)
        
    def projRadioChange(self, e):
        if self.projectileRadio.GetSelection()==0: #damage
            self.projComboBox.Clear()
            for dString in self.PDList:
                self.projComboBox.Append(dString)
            self.projComboBox.SetSelection(0)
            self.projComboBox.Show(True)
        elif self.projectileRadio.GetSelection()==1: #Inertia
            self.projComboBox.Clear()
            for dString in self.InertiaList:
                self.projComboBox.Append(dString)
            self.projComboBox.SetSelection(0)
            self.projComboBox.Show(True)
        elif self.projectileRadio.GetSelection()==2: #Class
            self.projComboBox.Clear()
            for dString in self.ClassList:
                self.projComboBox.Append(dString)
            self.projComboBox.SetSelection(0)
            self.projComboBox.Show(True)
        elif self.projectileRadio.GetSelection()==3: #PRGR
            self.projComboBox.Show(False)
        elif self.projectileRadio.GetSelection()==4: #Unused Projectiles
            self.projComboBox.Show(False)
        elif self.projectileRadio.GetSelection()==5: #Dormant Projectiles
            self.projComboBox.Clear()
            self.projComboBox.Append("Dormant")
            self.projComboBox.Append("Not Dormant")
            self.projComboBox.SetSelection(0)
            self.projComboBox.Show(True)
            
    def monsGo(self,e):
        localOnly=self.monsCheckbox.GetValue()
        if self.monsterRadio.GetSelection()==0:
            stringList=self.mTags.findAllDeadMonsters(self.newLocal, localOnly)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
        elif self.monsterRadio.GetSelection()==1:
            fullString=""
            if self.monsComboBox.GetCurrentSelection()==0:
                for i in range(1, len(self.monsList)):
                    mons=self.monsList[i]
                    titleData=[mons, self.monsList[i][1]]
                    addedTitle=False
                    stringList=self.mTags.findMonsByName(mons,self.newLocal, localOnly)
                    for tmpData in stringList:
                        if tmpData[0]!=-1:
                            if not addedTitle:
                                fullString=fullString+titleData[0]
                                addedTitle=True
                            fullString=fullString+"     "+tmpData[0]
                    if not addedTitle:
                        fullString="Couldn't find any Monsters"
            else:    
                stringList=self.mTags.findMonsByName(self.monsComboBox.GetValue(),self.newLocal, localOnly)
                for theText in stringList:
                    outText = theText[0]
                    if outText == -1:
                        outText="Couldn't find a Monster with that name"
                    fullString=fullString+outText
            self.control.SetValue(fullString)
        
    def unitGo(self,e):     
        localOnly=self.extractionCheckbox.GetValue()
        tagIndex = self.extractionRadio.GetSelection()   
        if tagIndex==0: 
            tagType="unit"
        elif tagIndex==1:
            tagType="arti"
        elif tagIndex==2:
            tagType="scen"
        elif tagIndex==3:
            tagType="stli"
        elif tagIndex==4:
            tagType="text"
        elif tagIndex==5:
            tagType="mesh"
        if tagIndex==3 or tagIndex==4:
            outString = ""
            tmpList = self.newLocal.getTagList(tagType, localOnly)
            try:
                self.control.SetValue("Gathering Data...")
                for tTag in tmpList:
                    outString+="(" + tTag.tagHeader.getID() + ") " + tTag.tagHeader.getName() + "\n"
                    outString+=tTag.theString + "\n\n"
                self.control.SetValue(outString)
            except:
                print 'Error:', sys.exec_type, ":", sys.exc_value
        else:
            fullString = "Gathering Related Tags\n"
            self.control.SetValue(fullString)
            tmpList=self.newLocal.getTagList(tagType, False)
            for tTag in tmpList:
                if tTag.getTagName()==self.extractComboBox.GetValue():
                    break
                    
            fullString = fullString+"Exporting " + tTag.tagHeader.name + "\n"
            self.mTags.ExportLinkedTags(self.newLocal, tTag, localOnly)
            fullString=fullString+"Exporting Successful\n"
            self.control.SetValue(fullString)
        
    def objeGo(self,e):    
        if self.newLocal.didError==False:
            if self.objeRadio.GetSelection()==0:
                localOnly=self.objeCheckbox.GetValue()
                stringList=self.mTags.findDuplicateObjects(self.newLocal,localOnly)
                fullString=""
                for tmpData in stringList:
                    fullString=fullString+tmpData[0]
            else:
                unusedTagList=self.mTags.findUnusedTags(self.newLocal, True)
                fullString=""
                for tmpTag in unusedTagList:
                    fullString+=tmpTag.tagHeader.getGroup_Tag() + " (" + tmpTag.tagHeader.getSubgroup_Tag() + ") " + tmpTag.tagHeader.getName() + "\n"  
            self.control.SetValue(fullString)
        else:
            self.control.SetValue(self.newLocal.ErrorMessage)
        
    def projGo(self,e):
        localOnly=self.projCheckbox.GetValue()
        if self.projectileRadio.GetSelection()==0: 
            stringList=self.mTags.listByDamageType(self.projComboBox.GetSelection(), self.PDList, self.newLocal, localOnly)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
        elif self.projectileRadio.GetSelection()==1: 
            stringList=self.mTags.listByInertiaType(self.projComboBox.GetSelection(), self.InertiaList, self.newLocal, localOnly)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
        elif self.projectileRadio.GetSelection()==2: 
            stringList=self.mTags.listByClassType(self.projComboBox.GetSelection(), self.ClassList, self.newLocal, localOnly)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
        elif self.projectileRadio.GetSelection()==3: 
            stringList=self.mTags.PrintPRGRandPROJ(self.newLocal,localOnly)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
        elif self.projectileRadio.GetSelection()==4:
            stringList=self.mTags.listUnusedProjectiles(self.newLocal,localOnly)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
        elif self.projectileRadio.GetSelection()==5:
            if self.projComboBox.GetSelection()==0:
                dormant = True
            else:
                dormant = False
            stringList=self.mTags.listDormantProjectilesState(self.newLocal,localOnly, dormant)
            fullString=""
            for theText in stringList:
                fullString=fullString+theText[0]
            self.control.SetValue(fullString)
            

app = wx.PySimpleApp()
try:
    tools_file = PATH_TO_TAGS + "/local/tools"
    if os.path.isfile(tools_file):
        tmp = file('/Users/mikeoldham/Documents/vc/Chaos/dist/local/tools', 'rb')
        tmp.close()
except IOError:
    wx.MessageBox('Cannot set exclusive mode. Fear or Loathing is already running, or the Myth directory is marked read-only.', 'Chaos Error', wx.ICON_ERROR | wx.OK)
else:
    frame=MainWindow(None,-1,'Chaos')

    frame.printToOutput("Updating tag list please wait...")
    frame.shouldAppend=True   
    frame.newLocal=localInfo(PATH_TO_TAGS)
    frame.refreshTags(frame.printToOutput)
    frame.printToOutput("Tag list updated.")
    frame.shouldAppend = False

    app.MainLoop()