# Requires wxPython.  This sample demonstrates:
#
# - single file exe using wxPython as GUI.

from distutils.core import setup
import py2exe

# setup(windows=['wxController.py'], 
    # options={ "py2exe":{"unbuffered": True,"optimize": 2,"excludes": ["email"]}})

setup(
    options = {'py2exe': {'bundle_files': 1}},
    windows = [{'script': "wxController.py", 
				'icon_resources':[(1, "Chaos.ico")],
				'dest_base':"Chaos",
			}],
	name = "Chaos",
	version = "1.2",
	description="Myth Diagnostics Tool",
	author="Mike Oldham",
    zipfile = None,
)