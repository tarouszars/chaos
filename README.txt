Some notes on getting this to compile.

### Mac ###
Python on OSX has some problems, most notably if you want to compile an app that runs on both 10.5 and 10.6 you cannot use the system supplied version of python. You need to download and install your own version.

To compile you run:
python setup.py py2app
