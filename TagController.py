import objc
from Foundation import *
from AppKit import *
from PyObjCTools import AppHelper
from mythTags import *


# NibClassBuilder.extractClasses("MainMenu")


# class defined in MainMenu.nib
class MonsterController(NSObject):
    # the actual base class is NSObject
    # The following outlets are added to the class:
    ExtractionLocalCheckbox = objc.IBOutlet()
    LoaderText = objc.IBOutlet()
    LoadWindow = objc.IBOutlet()
    MainChaosWindow = objc.IBOutlet()
    MonsterLocalCheckbox = objc.IBOutlet()
    MonsterPopup = objc.IBOutlet()
    MonsterRadio = objc.IBOutlet()
    MonsterTextField = objc.IBOutlet()
    ObjectLocalCheckbox = objc.IBOutlet()
    ObjectRadio = objc.IBOutlet()
    PluginPopup = objc.IBOutlet()
    progressBar = objc.IBOutlet()
    progressSpinner = objc.IBOutlet()
    ProjectileLocalCheckbox = objc.IBOutlet()
    ProjectilePopup = objc.IBOutlet()
    ProjectileRadio = objc.IBOutlet()
    theOutput = objc.IBOutlet()
    UnitPopup = objc.IBOutlet()
    UnitRadio = objc.IBOutlet()
    
    #def windowDidLoad(self):
    def __init__(self):
        self.pluginName = ""
        
    def monsterChooseRadio_(self, sender):
        theCell=sender.selectedCell()
        theTag=theCell.tag()
        if theTag==0:
            self.MonsterPopup.setHidden_(True)
            #self.MonsterLocalCheckbox.setHidden_(False)
        else: 
            self.MonsterPopup.setHidden_(False)
            #self.MonsterLocalCheckbox.setHidden_(True)
            
    def monsterGoButton_(self, sender):
        if self.newLocal.didError==False:
            if self.MonsterLocalCheckbox.state()==1:
                localOnly=True
            else:
                localOnly=False
            if self.MonsterRadio.cellAtRow_column_(0,0).state()==1:
                stringList=self.mTags.findAllDeadMonsters(self.newLocal, localOnly)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.MonsterRadio.cellAtRow_column_(1,0).state()==1:    
                tagString= NSMutableAttributedString.alloc().init()
                if self.MonsterPopup.indexOfSelectedItem()==0:
                    for i in range(1,len(self.monsList)):
                        mons=self.monsList[i]
                        titleData=[mons, self.monsList[i][1]]
                        addedTitle=False
                        stringList=self.mTags.findMonsByName(mons,self.newLocal, localOnly)
                        for tmpData in stringList:
                            if tmpData[0]!=-1:
                                if not addedTitle:
                                    tagString.appendAttributedString_(self.createColoredText(titleData[0]+"\n", titleData[1]))
                                    addedTitle=True
                                tagString.appendAttributedString_(self.createColoredText("     "+tmpData[0], tmpData[1]))
                else:
                    stringList=self.mTags.findMonsByName(self.monsList[self.MonsterPopup.indexOfSelectedItem()],self.newLocal, False)
                    for tmpData in stringList:
                        if tmpData[0]==-1:
                            tmpData[0]="Couldn't find a Monster with that name"
                        tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.MonsterRadio.cellAtRow_column_(2,0).state()==1:
                stringList=self.mTags.findMonsByTag(str(self.MonsterTextField.stringValue()).rstrip(),self.newLocal)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            else:
                self.theOutput.textStorage().setAttributedString_(self.createColoredText("You broke something noob!~", 3))
        else:
            self.theOutput.textStorage().setAttributedString_(self.createColoredText(self.newLocal.ErrorMessage,3))
    
    
    def projectileChooseRadio_(self, sender):
        theCell=sender.selectedCell()
        theTag=theCell.tag()
        if theTag==0:
            self.ProjectilePopup.setHidden_(False)
            self.ProjectilePopup.removeAllItems()
            self.ProjectilePopup.addItemsWithTitles_(self.PDList)
        elif theTag==1: 
            self.ProjectilePopup.setHidden_(False)
            self.ProjectilePopup.removeAllItems()
            self.ProjectilePopup.addItemsWithTitles_(self.InertiaList)
        elif theTag==2: 
            self.ProjectilePopup.setHidden_(False)
            self.ProjectilePopup.removeAllItems()
            self.ProjectilePopup.addItemsWithTitles_(self.ClassList)
        elif theTag==3: 
            self.ProjectilePopup.setHidden_(True)
        elif theTag==4: 
            self.ProjectilePopup.setHidden_(True)
        elif theTag==5:
            self.ProjectilePopup.setHidden_(False)
            self.ProjectilePopup.removeAllItems()
            self.ProjectilePopup.addItemsWithTitles_(["Dormant", "Not Dormant"])

    def projectileGoButton_(self, sender):
        self.progressSpinner.startAnimation_(sender)
        if self.newLocal.didError==False:
            if self.ProjectileLocalCheckbox.state()==1:
                localOnly=True
            else:
                localOnly=False
            if self.ProjectileRadio.cellAtRow_column_(0,0).state()==1:
                stringList=self.mTags.listByDamageType(self.ProjectilePopup.indexOfSelectedItem(), self.PDList, self.newLocal,localOnly)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.ProjectileRadio.cellAtRow_column_(1,0).state()==1:
                stringList=self.mTags.listByInertiaType(self.ProjectilePopup.indexOfSelectedItem(), self.InertiaList, self.newLocal,localOnly)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.ProjectileRadio.cellAtRow_column_(2,0).state()==1:
                stringList=self.mTags.listByClassType(self.ProjectilePopup.indexOfSelectedItem(), self.ClassList, self.newLocal,localOnly)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.ProjectileRadio.cellAtRow_column_(3,0).state()==1:
                stringList=self.mTags.PrintPRGRandPROJ(self.newLocal,localOnly)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.ProjectileRadio.cellAtRow_column_(4,0).state()==1:
                stringList=self.mTags.listUnusedProjectiles(self.newLocal,localOnly)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.ProjectileRadio.cellAtRow_column_(5,0).state()==1:
                if self.ProjectilePopup.indexOfSelectedItem()==0:
                    dormant = True
                else:
                    dormant = False
                stringList=self.mTags.listDormantProjectilesState(self.newLocal,localOnly, dormant)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpData in stringList:
                    tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                self.theOutput.textStorage().setAttributedString_(tagString)
        else:
            self.theOutput.textStorage().setAttributedString_(self.createColoredText(self.newLocal.ErrorMessage,3))
        self.progressSpinner.stopAnimation_(sender)
    
    def objectChooseRadio_(self, sender):
        theCell=sender.selectedCell()
        theTag=theCell.tag()
        if theTag==0:
            self.ObjectLocalCheckbox.setHidden_(False)
        elif theTag==1:
            self.ObjectLocalCheckbox.setHidden_(True)
            self.printToOutput("Hit Go to find which tags in your local folder are not being preloaded into your meshes \n\n(You must have created at least one preloaded data tag by holding the spacebar while starting your mesh.)")

    def objectGoButton_(self, sender):        
        self.progressSpinner.startAnimation_(sender)
        if self.newLocal.didError==False:
            if self.ObjectRadio.cellAtRow_column_(1,0).state()==1:
                unusedTagList=self.mTags.findUnusedTags(self.newLocal, True)
                tagString= NSMutableAttributedString.alloc().init()
                for tmpTag in unusedTagList:
                    tagString.appendAttributedString_(self.createColoredText(tmpTag.tagHeader.getGroup_Tag() + " (" + tmpTag.tagHeader.getSubgroup_Tag() + ") " + tmpTag.tagHeader.getName() + "\n", tmpTag.tagHeader.getTagType()))
                self.theOutput.textStorage().setAttributedString_(tagString)
            elif self.ObjectRadio.cellAtRow_column_(0,0).state()==1:
                if self.newLocal.didError==False:
                    if self.ObjectLocalCheckbox.state()==1:
                        localOnly=True
                    else:
                        localOnly=False
                    if self.ObjectRadio.cellAtRow_column_(0,0).state()==1:
                        stringList=self.mTags.findDuplicateObjects(self.newLocal,localOnly)
                        tagString= NSMutableAttributedString.alloc().init()
                        for tmpData in stringList:
                            tagString.appendAttributedString_(self.createColoredText(tmpData[0], tmpData[1]))
                        self.theOutput.textStorage().setAttributedString_(tagString)
                else:
                    self.theOutput.textStorage().setAttributedString_(self.createColoredText(self.newLocal.ErrorMessage,3))
        self.progressSpinner.stopAnimation_(sender)
    
    def unitChooseRadio_(self, sender):
        theCell=sender.selectedCell()
        theTag=theCell.tag()
        if theTag==0:        
            tagType="unit"
        elif theTag==1: 
            tagType="arti"
        elif theTag==2: 
            tagType="scen"
        elif theTag==5: 
            tagType="mesh"
        if theTag < 3 or theTag == 5:
            tmpList=self.newLocal.getTagList(tagType, False)
            self.extractList=[]
            for eTag in tmpList:
                self.extractList.append(eTag.getTagName())
            if len(self.extractList)==0:
                self.extractList.append("Nothing to Extract")
            else:
                self.extractList.sort()
            self.UnitPopup.removeAllItems()
            self.UnitPopup.addItemsWithTitles_(self.extractList)
            self.UnitPopup.setHidden_(False)
        else: 
            self.UnitPopup.setHidden_(True)
        
    def unitGoButton_(self, sender):
        self.progressSpinner.startAnimation_(sender)
        tagString= NSMutableAttributedString.alloc().init()
        if self.UnitRadio.cellAtRow_column_(0,0).state()==1:
            tagType="unit"
        elif self.UnitRadio.cellAtRow_column_(1,0).state()==1:
            tagType="arti"
        elif self.UnitRadio.cellAtRow_column_(2,0).state()==1:
            tagType="scen"
        elif self.UnitRadio.cellAtRow_column_(3,0).state()==1:
            tagType="stli"
        elif self.UnitRadio.cellAtRow_column_(4,0).state()==1:
            tagType="text"
        elif self.UnitRadio.cellAtRow_column_(5,0).state()==1:
            tagType="mesh"
        if (self.UnitRadio.cellAtRow_column_(3,0).state()==1 or self.UnitRadio.cellAtRow_column_(4,0).state()==1):
            self.printToOutput("")
            localOnly=False
            if self.ExtractionLocalCheckbox.state()==1:
                localOnly=True
            tmpList=self.newLocal.getTagList(tagType, localOnly)
            self.shouldAppend=True
            try:
                for tTag in tmpList:
                    self.printToOutput("(" + tTag.tagHeader.getID() + ") " + tTag.tagHeader.getName(), tTag.tagHeader.getTagType())
                    self.printToOutput(tTag.theString, tTag.tagHeader.getTagType())
                    self.printToOutput("")
            except:
                print 'Error:', sys.exc_type, ":", sys.exc_value
                
        else:
            tagString.appendAttributedString_(self.createColoredText("Gathering Related Tags\n", 3))
            self.theOutput.textStorage().setAttributedString_(tagString)
            tmpList=self.newLocal.getTagList(tagType, False)
            for tTag in tmpList:
                if tTag.getTagName()==self.extractList[self.UnitPopup.indexOfSelectedItem()]:
                    break
            self.printToOutput("Exporting " + tagType + ": " + tTag.getTagName())
            self.shouldAppend=True
            if self.ExtractionLocalCheckbox.state()==1:
                localOnly=True
            else:
                localOnly=False
            self.mTags.ExportLinkedTags(self.newLocal, tTag, localOnly)
            self.printToOutput("Exporting Successful")
        self.shouldAppend=False
        self.progressSpinner.stopAnimation_(sender)
            
    
    def awakeFromNib(self):
        self.progressBar.setUsesThreadedAnimation_(True)
        self.progressBar.startAnimation_(self.LoadWindow)
        self.LoadWindow.makeKeyAndOrderFront_(self.LoadWindow)
        
        self.PDList=["None", "Explosive", "Magic", "Holding", "Healing", "Kenetic", "Slashing", "Stoning", "Slash Claws", "Explosive Electricity", "Fire", "Gas", "Charm"]
        self.InertiaList=["None", "Unwieldy", "Small", "Large"]
        self.ClassList=["None", "Unknown", "Head", "Arm", "Leg", "Torso", "Bloody Chunk", "Lethal Object", "Nonlethal Object", "Explosive Object", "Potentially Explosive Object", "Item"]
        self.ProjectilePopup.removeAllItems()
        self.ProjectilePopup.addItemsWithTitles_(self.PDList)
        self.newLocal=localInfo("../../../")
        self.refreshTags(self.printToDialog)
        self.shouldAppend=False
        
        self.LoadWindow.orderOut_(self.LoadWindow)
        self.progressBar.stopAnimation_(self.LoadWindow)
        self.MainChaosWindow.makeKeyAndOrderFront_(self.MainChaosWindow)
        
    def printToDialog(self, theString):
        self.LoaderText.setStringValue_(theString)
        self.LoaderText.display()
    
    def printToOutput(self, theString, theColor=3):
        if self.shouldAppend:
            self.theOutput.textStorage().appendAttributedString_(self.createColoredText("\n" + theString,theColor))
        else:
            self.theOutput.textStorage().setAttributedString_(self.createColoredText(theString,theColor))
        self.theOutput.display()
    
    
    def refreshTags(self, printFunction, pluginName=""):
        try:
            self.mTags=mythTags()
            self.newLocal.loadTags(printFunction, pluginName)
        
            lastIndex = self.PluginPopup.indexOfSelectedItem()
            self.PluginPopup.removeAllItems()
            self.PluginPopup.addItemsWithTitles_(["None"])
            self.PluginPopup.addItemsWithTitles_(self.newLocal.pluginList)
            self.PluginPopup.selectItemAtIndex_(lastIndex)
        
            tmpList=self.newLocal.getTagList('mons', False)
            self.monsList=[]
            for mons in tmpList:
                self.monsList.append(mons.getmName())
            self.monsList.sort()
            self.MonsterPopup.removeAllItems()
            self.monsList.insert(0, "All")
            self.MonsterPopup.addItemsWithTitles_(self.monsList)
        
            if self.UnitRadio.cellAtRow_column_(0,0).state()==1:
                tagType="unit"
            elif self.UnitRadio.cellAtRow_column_(1,0).state()==1:
                tagType="arti"
            elif self.UnitRadio.cellAtRow_column_(2,0).state()==1:
                tagType="scen"
            elif self.UnitRadio.cellAtRow_column_(5,0).state()==1:
                tagType="mesh"
            tmpList=self.newLocal.getTagList(tagType, False)
            self.extractList=[]
            for eTag in tmpList:
                self.extractList.append(eTag.tagHeader.name)
            self.extractList.sort()
            self.UnitPopup.removeAllItems()
            self.UnitPopup.addItemsWithTitles_(self.extractList)
        except:
            print 'Error:', sys.exc_type, ":", sys.exc_value
    
    def refreshButton_(self, sender):
        self.progressSpinner.startAnimation_(sender)
        self.printToOutput("Updating tag list please wait...")
        self.shouldAppend=True
        pluginName = ""
        if self.PluginPopup.indexOfSelectedItem()!=0:
           pluginName = self.PluginPopup.titleOfSelectedItem()
        self.refreshTags(self.printToOutput, pluginName)
        self.printToOutput("Tag list updated.")
        self.progressSpinner.stopAnimation_(sender)
        self.shouldAppend=False
        
    def createBoldColoredText(self, theText, theTagType):
        if theTagType==1:
            myDictionary = NSMutableDictionary.dictionaryWithObject_forKey_(NSColor.colorWithCalibratedRed_green_blue_alpha_(0.0,0.5,0.0,1.0), NSForegroundColorAttributeName)
        elif theTagType==2:
            myDictionary = NSMutableDictionary.dictionaryWithObject_forKey_(NSColor.redColor(), NSForegroundColorAttributeName)
        else:
            myDictionary = NSMutableDictionary.dictionaryWithObject_forKey_(NSColor.blackColor(), NSForegroundColorAttributeName)
        myDictionary.setObject_forKey_(NSFont.boldSystemFontOfSize_(12), NSFontAttributeName)
        attString=NSAttributedString.alloc().initWithString_attributes_(theText, myDictionary)
        return attString
        
    def createColoredText(self, theText, theTagType):
        if theTagType==0:
            myDictionary = NSDictionary.dictionaryWithObject_forKey_(NSColor.colorWithCalibratedRed_green_blue_alpha_(0.0,0.5,0.0,1.0), NSForegroundColorAttributeName)
        elif theTagType==1:
            myDictionary = NSDictionary.dictionaryWithObject_forKey_(NSColor.colorWithCalibratedRed_green_blue_alpha_(0.35,0.42,0.996,1.0), NSForegroundColorAttributeName)
        elif theTagType==2:
            myDictionary = NSDictionary.dictionaryWithObject_forKey_(NSColor.redColor(), NSForegroundColorAttributeName)
        else:
            myDictionary = NSDictionary.dictionaryWithObject_forKey_(NSColor.blackColor(), NSForegroundColorAttributeName)
        attString=NSAttributedString.alloc().initWithString_attributes_(theText, myDictionary)
        return attString        
        

if __name__ == "__main__":
    AppHelper.runEventLoop()
