#!/usr/bin/python

__author__ = "Mike Oldham (tarous@gmail.com)"
__version__ = "Revision: 1.0"
__date__ = "Date: 2006/04/27"
__copyright__ = "Copyright (c) 2006 Mike Oldham"
__license__ = "CC"

import struct

def FLAG(a): return (1<<a)

def writeString(rString, slen, max):
    if slen>max:
        appendString=""
        for i in range(max):
            appendString+=rString[i]
        return appendString
    dlen = max-slen
    appendString=rString
    if dlen%2==1:
        dlen=dlen+1
        sub1=True
    else:
        sub1=False
    for i in range(dlen/2):
        d=struct.pack(">h", 0)
        e=struct.unpack(">2s", d)[0]
        if i==0 and sub1:
            e=e[0]
        appendString+=e
    return appendString
        

def readString(rString):
    d=struct.pack(">h", 0)
    e=struct.unpack(">2s", d)[0]
    gString=""
    for i in rString:
        if i != e[0]:
            gString=gString+i
        else:
            break
    return gString

def tagCheck(theTag):
    if struct.pack(">L", theTag)=='\xff\xff\xff\xff':
        return "None"
    else:
        return struct.pack(">L", theTag)

def prepareTag(theTag):
    return struct.pack(">L", theTag)

def prepareString(theString):
    if theString=="None":
        return struct.unpack(">L", '\xff\xff\xff\xff')[0]
    else:
        if len(theString)==struct.calcsize(">L"):
            return struct.unpack(">L", str(theString))[0]
        else:
            return False
    

class tag_file_header:
    def __init__(self,dFile):
        self.type=struct.unpack(">h", dFile.read(2))[0]
        self.version=struct.unpack(">h", dFile.read(2))[0]
        self.name=readString(dFile.read(32))
        self.url=readString(dFile.read(64))
        self.entry_point_count=struct.unpack(">h", dFile.read(2))[0]
        self.tag_count=struct.unpack(">h", dFile.read(2))[0]
        self.checksum=struct.unpack(">L", dFile.read(4))[0]
        self.flags=struct.unpack(">L", dFile.read(4))[0]
        self.size=struct.unpack(">l", dFile.read(4))[0]
        self.header_checksum=struct.unpack(">L", dFile.read(4))[0]
        self.unused=struct.unpack(">l", dFile.read(4))[0]
        self.signature=struct.unpack(">L", dFile.read(4))[0]
        
class Tag_Header:
    """Default 64 byte tag header for all myth tags."""
    def __init__(self,dFile,dOffset):
        dFile.seek(dOffset)
        self.Identifier=struct.unpack(">h", dFile.read(2))[0]
        self.flags=struct.unpack("c", dFile.read(1))[0]
        self.type=struct.unpack("c", dFile.read(1))[0]
        self.name=readString(dFile.read(32))
        self.group_tag=dFile.read(4)
        self.subgroup_tag=dFile.read(4)     #aka 4 byte tag ID
        self.offset=struct.unpack(">i", dFile.read(4))[0]
        self.size=struct.unpack(">l", dFile.read(4))[0]
        self.user_data=struct.unpack(">L", dFile.read(4))[0]
        self.version=struct.unpack(">h", dFile.read(2))[0]
        self.destination=struct.unpack("c", dFile.read(1))[0]
        self.owner_index=struct.unpack("c", dFile.read(1))[0]
        self.game_version=dFile.read(4)
        self.tagType=0
    def setTagType(self, theType): self.tagType=theType
    def getTagType(self): return self.tagType
    def getOffset(self): return self.offset
    def getName(self): return self.name
    def getGroup_Tag(self): return self.group_tag
    def getSubgroup_Tag(self): return self.subgroup_tag
    def getID(self): return self.subgroup_tag
    def setName(self, name): self.name=name
    def setSubgroup_Tag(self, subgroup_tag): self.subgroup_tag=subgroup_tag
    def writeHeader(self, dFile, dOffset, localOffset=False):
        dFile.seek(dOffset)
        if (localOffset):
            dFile.write(struct.pack(">h", 0))
            dFile.write(struct.pack(">c", '\0'))
            dFile.write(struct.pack(">c",  '\0'))
        else:
            dFile.write(struct.pack(">h", self.Identifier))
            dFile.write(struct.pack(">c", self.flags))
            dFile.write(struct.pack(">c",  self.type))
        dFile.write(writeString(self.name, len(self.name), 32))
        dFile.write(writeString(self.group_tag, len(self.name), 4))
        dFile.write(writeString(self.subgroup_tag, len(self.name), 4))     #aka 4 byte tag ID
        if (localOffset):
            dFile.write(struct.pack(">i", 64))
        else:
            dFile.write(struct.pack(">i", self.offset))
        dFile.write(struct.pack(">l", self.size))
        dFile.write(struct.pack(">L", self.user_data))
        dFile.write(struct.pack(">h", self.version))
        dFile.write(struct.pack(">c", self.destination))
        dFile.write(struct.pack(">c", self.owner_index))
        dFile.write(writeString(self.game_version, len(self.name), 4))
       