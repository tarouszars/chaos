import tag_header,struct

#struct bitmap_preload_data
#{
#	file_tag collection_tag;
#
#	short bitmap_index;
#	short pad;
#};
#
#struct preload_data
#{
#	file_tag group;
#	file_tag subgroup;
#};

#define SIZEOF_STRUCT_PRELOAD_DEFINITION 32
#struct preload_definition
#{
#	long preload_data_count, preload_data_offset, preload_data_size;
#	struct preload_data *data;
#
#	long bitmap_preload_data_count, bitmap_preload_data_offset, bitmap_preload_data_size;
#	struct bitmap_preload_data *bitmap_data;
#};


class PREL:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			dFile.seek(self.tagHeader.getOffset())
			self.isLocal=False
		self.preload_data_count=struct.unpack(">L", dFile.read(4))[0]	
		self.preload_data_offset=struct.unpack(">L", dFile.read(4))[0]
		self.preload_data_size=struct.unpack(">L", dFile.read(4))[0]
		unknown=dFile.read(20)
		self.preload_definition={}
		for i in range(self.preload_data_count):
			tag_type=dFile.read(4)
			tag_name=dFile.read(4)
			if tag_type in self.preload_definition:
				tag_list=self.preload_definition[tag_type]
				tag_list.append(tag_name)
				self.preload_definition[tag_type]=tag_list
			else:
				self.preload_definition[tag_type]=[tag_name]
		dFile.close
		
		
	def getLinkedTags(self):
		return False