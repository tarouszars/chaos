import tag_header,struct

class TEXT:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
            self.theString = tag_header.readString(dFile.read())
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
            self.theString = tag_header.readString(dFile.read(self.tagHeader.size))    
    
    def getLinkedTags(self):
        return False
    
