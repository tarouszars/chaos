import tag_header,struct

class SCEN:
	def __init__(self,dFile,dOffset,localFile, filePath):
		MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION=4
		MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION=6
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			self.isLocal=False
			dFile.seek(self.tagHeader.getOffset())
		
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		
		self.collection_reference_tag=struct.unpack(">L", dFile.read(4))[0]   #file_tag so I need to pack before reading
		
		self.object_tag=struct.unpack(">L", dFile.read(4))[0]   #file_tag
		self.projectile_tag=struct.unpack(">L", dFile.read(4))[0]   #file_tag
		
		self.valid_netgame_scoring_type=struct.unpack(">h", dFile.read(2))[0] 
		self.netgame_flag_number=struct.unpack(">h", dFile.read(2))[0] 
		
		unused=dFile.read(4)
		self.projectile_group_tags=[]
		for i in range(MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION):
			self.projectile_group_tags.append(struct.unpack(">L", dFile.read(4))[0])
		self.sequence_indexes=[]
		for i in range(MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION):
			self.sequence_indexes.append(struct.unpack(">h", dFile.read(2))[0])
		self.model_permutation_delta=struct.unpack(">h", dFile.read(2))[0]
		unused2=dFile.read(58)
		self.projectile_group_types=[]
		for i in range(MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION):
			self.projectile_group_types.append(struct.unpack(">h", dFile.read(2))[0])
		self.collection_reference_index=struct.unpack(">h", dFile.read(2))[0]
		self.impact_projectile_group_type=struct.unpack(">h", dFile.read(2))[0]
		self.object_type=struct.unpack(">h", dFile.read(2))[0]
		self.projectile_type=struct.unpack(">h", dFile.read(2))[0]
		
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.collection_reference_tag) != "None":
			tmpList.append((self.collection_reference_tag, 'core'))
		if tag_header.tagCheck(self.object_tag) != "None":
			tmpList.append((self.object_tag, 'obje'))
		if tag_header.tagCheck(self.projectile_tag) != "None":
			tmpList.append((self.projectile_tag, 'proj'))
		for dProjectileGroup in self.projectile_group_tags:
			if tag_header.tagCheck(dProjectileGroup) != "None":
				tmpList.append((dProjectileGroup, 'prgr'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False
		
	def getTagName(self):
		return self.tagHeader.name