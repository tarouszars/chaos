import tag_header,struct

class UNIT:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			dFile.seek(64)
			self.isLocal=True
		else:
			dFile.seek(self.tagHeader.getOffset())
			self.isLocal=False
		self.monsCode=struct.unpack(">L", dFile.read(4))[0]
		self.crefCode=struct.unpack(">L", dFile.read(4))[0]
	def getTagName(self): return self.tagHeader.getName()
	def getUNITName(self): return self.tagHeader.getName()
	def getUNITCode(self): return self.tagHeader.getSubgroup_Tag()
	def getMONSCode(self): return self.monsCode
	def getCREFCode(self): return self.crefCode
	def getIsLocal(self): return self.isLocal
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.monsCode) != "None":
			tmpList.append((self.monsCode, 'mons'))
		if tag_header.tagCheck(self.crefCode) != "None":
			tmpList.append((self.crefCode, 'core'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False