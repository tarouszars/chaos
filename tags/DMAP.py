import tag_header,struct


class detail_map_entry:
	def __init__(self,dFile):
		self.dtex_id=struct.unpack(">L", dFile.read(4))[0]
		self.pixels_per_cell=dFile.read(1)
		self.r=dFile.read(1)
		self.g=dFile.read(1)
		self.b=dFile.read(1)
		self.unused=dFile.read(24)
		self.name=dFile.read(32)

#detail_map_definition
class DMAP:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		MAX_DTEX_TAGS_PER_DMAP=128
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			dFile.seek(self.tagHeader.getOffset())
			self.isLocal=False
		self.width = struct.unpack(">I", dFile.read(4))[0]
		self.height = struct.unpack(">I", dFile.read(4))[0]
		self.v_indices_offset = struct.unpack(">l", dFile.read(4))[0]
		self.v_indices = dFile.read(4)
		self.t_indices_offset = struct.unpack(">l", dFile.read(4))[0]
		self.t_indices = dFile.read(4)
		self.entries_offset = struct.unpack(">l", dFile.read(4))[0]
		currentOffset=dFile.tell()
		self.entries=[]
		dFile.seek(self.tagHeader.getOffset()+self.entries_offset)
		for i in range(MAX_DTEX_TAGS_PER_DMAP):
			entry = detail_map_entry(dFile)
			if tag_header.tagCheck(entry.dtex_id)!='None':
				self.entries.append(entry)
		dFile.seek(currentOffset)
		self.unused=dFile.read(224)
		
		
	def getLinkedTags(self):
		result=[]
		for entry in self.entries:
			if tag_header.tagCheck(entry.dtex_id) != "None":
				result.append((entry.dtex_id, 'dtex'))
		return result