import tag_header,struct

MAXIMUM_FRAMES_PER_ANIMATION=31
class model_animation_frame_definition:
    def __init__(self,dFile):
        self.flags = struct.unpack(">L", dFile.read(4))[0]
        self.model_tag = struct.unpack(">L", dFile.read(4))[0]
        self.model_type = struct.unpack(">h", dFile.read(2))[0]
        self.permutation_index = struct.unpack(">h", dFile.read(2))[0]
        self.unused = dFile.read(4)
    
class ANIM:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        self.flags = struct.unpack(">L", dFile.read(4))[0]
        self.number_of_frames = struct.unpack(">h", dFile.read(2))[0]
        self.ticks_per_frame = struct.unpack(">h", dFile.read(2))[0]
        
        self.model_animation_frame_definition= []
        for i in range(MAXIMUM_FRAMES_PER_ANIMATION):
            self.model_animation_frame_definition.append(model_animation_frame_definition(dFile))
        self.unused=dFile.read(222*2)

        self.shadow_map_width = struct.unpack(">h", dFile.read(2))[0]
        self.shadow_map_height = struct.unpack(">h", dFile.read(2))[0]
        self.shadow_bytes_per_row = struct.unpack(">h", dFile.read(2))[0]
        self.pad = struct.unpack(">h", dFile.read(2))[0]
        self.origin_offset_x = struct.unpack(">h", dFile.read(2))[0]
        self.origin_offset_y = struct.unpack(">h", dFile.read(2))[0]
        
        self.forward_sound_tag = struct.unpack(">L", dFile.read(4))[0]
        self.backward_sound_tag = struct.unpack(">L", dFile.read(4))[0]
    
        self.forward_sound_type = struct.unpack(">h", dFile.read(2))[0]
        self.backward_sound_type = struct.unpack(">h", dFile.read(2))[0]
    
        self.unused2 = dFile.read(20*2)

        self.shadow_maps_offset = struct.unpack(">l", dFile.read(4))[0]
        self.shadow_maps_size = struct.unpack(">l", dFile.read(4))[0]
        self.shadow_maps = dFile.read(4)
        
    def getLinkedTags(self):
        result = [(self.forward_sound_tag, "soun"), (self.backward_sound_tag, "soun")]
        for i in range(self.number_of_frames):
            result.append((self.model_animation_frame_definition[i].model_tag,"mode"))
        return result