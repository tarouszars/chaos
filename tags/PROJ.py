import tag_header,struct
from tag_header import FLAG

class damage_definition:																		#len 16
	def __init__(self,dFile):
		self.type=struct.unpack(">H", dFile.read(2))[0]
		self.flags=struct.unpack(">H", dFile.read(2))[0]
		self.damage_lower_bound=struct.unpack(">h", dFile.read(2))[0]
		self.damage_delta=struct.unpack(">h", dFile.read(2))[0]
		self.radius_lower_bound=struct.unpack(">h", dFile.read(2))[0]
		self.radius_delta=struct.unpack(">h", dFile.read(2))[0]
		self.rate_of_expansion=struct.unpack(">h", dFile.read(2))[0]
		self.damage_to_velocity=struct.unpack(">h", dFile.read(2))[0]

class PROJ:																						#len 320
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SEQUENCES= 3
		MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SOUNDS=4
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			self.isLocal=False									
			dFile.seek(self.tagHeader.getOffset())												#64
		self.flags=struct.unpack(">L", dFile.read(4))[0]										#68
		self.collection_tag=struct.unpack(">L", dFile.read(4))[0]								#coll#72
		self.sequence_indexes=[]
		for i in range(MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SEQUENCES):						#78
			self.sequence_indexes.append(struct.unpack(">h", dFile.read(2))[0])					#80
		self.splash_or_rebound_effect_type=struct.unpack(">h", dFile.read(2))[0]				#82
		self.detonation_projectile_group_tag=struct.unpack(">L", dFile.read(4))[0]				#86
		self.contrail_projectile_tag=struct.unpack(">L", dFile.read(4))[0]			#90
		self.ticks_between_contrails=struct.unpack(">h", dFile.read(2))[0]						#92
		self.maximum_contrail_count=struct.unpack(">h", dFile.read(2))[0]						#94
		self.object_tag=struct.unpack(">L", dFile.read(4))[0]									#obje#98
		self.inertia_lower_bound=struct.unpack(">h", dFile.read(2))[0]							#100
		self.inertia_delta=struct.unpack(">h", dFile.read(2))[0]								#102
		self.random_initial_velocity=struct.unpack(">h", dFile.read(2))[0]						#104
		self.volume=struct.unpack(">h", dFile.read(2))[0]										#106
		self.tracking_priority=struct.unpack(">H", dFile.read(2))[0]							#108
		self.promotion_on_detonation_fraction=struct.unpack(">H", dFile.read(2))[0]				#110
		self.detonation_frequency=struct.unpack(">H", dFile.read(2))[0]							#112
		self.media_detonation_frequency=struct.unpack(">H", dFile.read(2))[0]					#114
		self.detonation_velocity=struct.unpack(">H", dFile.read(2))[0]							#116
		self.projectile_class=struct.unpack(">H", dFile.read(2))[0]								#118
		self.lightning_tag=struct.unpack(">L", dFile.read(4))[0]								#ligh#122
		unused=dFile.read(4)																	#126
		self.sound_tags=[]
		for i in range(MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SOUNDS):
 			self.sound_tags.append(struct.unpack(">L", dFile.read(4))[0])						#soun#142
		self.animation_rate_lower_bound=struct.unpack(">h", dFile.read(2))[0]					#144
		self.animation_rate_delta=struct.unpack(">h", dFile.read(2))[0]							#146
		self.lifespan_lower_bound=struct.unpack(">h", dFile.read(2))[0] 						#148
		self.lifespan_delta=struct.unpack(">h", dFile.read(2))[0]								#150
		self.initial_contrail_frequency=struct.unpack(">H", dFile.read(2))[0] 					#152
		self.final_contrail_frequency=struct.unpack(">H", dFile.read(2))[0]						#154
		self.contrail_frequency_delta=struct.unpack(">H", dFile.read(2))[0]						#156
		self.guided_turning_speed=struct.unpack(">H", dFile.read(2))[0]							#158
		self.promoted_projectile_tag=struct.unpack(">L", dFile.read(4))[0]						#proj#162
		self.promotion_projectile_group_tag=struct.unpack(">L", dFile.read(4))[0]				#prgr#164
		self.maximum_safe_acceleration=struct.unpack(">H", dFile.read(2))[0];
		self.acceleration_detonation_fraction=struct.unpack(">H", dFile.read(2))[0]	    		#168
		self.damage=damage_definition(dFile)													#184
		self.artifact_tag=struct.unpack(">L", dFile.read(4))[0]									#arti#188
		self.target_detonation_projectile_group_tag=struct.unpack(">L", dFile.read(4))[0]		#prgr#192
		self.geometry_tag=struct.unpack(">L", dFile.read(4))[0]			#196
		self.delay_lower_bound=struct.unpack(">h", dFile.read(2))[0]							#198
		self.delay_delta=struct.unpack(">h", dFile.read(2))[0]									#200
		self.local_projectile_group_tag=struct.unpack(">L", dFile.read(4))[0]					#204
		self.nearby_target_radius=struct.unpack(">h", dFile.read(2))[0]							#206
		self.exclude_nearby_target_radius=struct.unpack(">h", dFile.read(2))[0]							#208
		self.promotion_unit_tag=struct.unpack(">L", dFile.read(4))[0]					#210
		unused3=dFile.read(74)																	#214
		unused3c=struct.unpack(">c", dFile.read(1))[0]											#284
		self.use_accel=struct.unpack(">c", dFile.read(1))[0]	
		
		
		
	def getPROJName(self): return self.tagHeader.getName()
	def getPROJCode(self): return self.tagHeader.getSubgroup_Tag()
	def getIsLocal(self): return self.isLocal
	def getPROJGroup(self): return self.tagHeader.getGroup_Tag()
	def getInertiaType(self): return self.volume
	def getClassType(self): return self.projectile_class
	def isDormant(self):
		dormant_bit = 8
		dormant_flag = FLAG(dormant_bit)
		if (self.flags&dormant_flag > 0):
			return True
		else:
			return False	
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.collection_tag) != "None":
			tmpList.append((self.collection_tag, '.256'))
		if tag_header.tagCheck(self.detonation_projectile_group_tag) != "None":
			tmpList.append((self.detonation_projectile_group_tag, 'prgr'))
		if tag_header.tagCheck(self.contrail_projectile_tag) != "None":
			tmpList.append((self.contrail_projectile_tag, 'proj'))
		if tag_header.tagCheck(self.object_tag) != "None":
			tmpList.append((self.object_tag, 'obje'))
		if tag_header.tagCheck(self.lightning_tag) != "None":
			tmpList.append((self.lightning_tag, 'ligh'))
		for dSound in self.sound_tags:
			if tag_header.tagCheck(dSound) != "None":
				tmpList.append((dSound, 'soun'))
		if tag_header.tagCheck(self.promoted_projectile_tag) != "None":
			tmpList.append((self.promoted_projectile_tag, 'proj'))
		if tag_header.tagCheck(self.promotion_projectile_group_tag) != "None":
			tmpList.append((self.promotion_projectile_group_tag, 'prgr'))
		if tag_header.tagCheck(self.artifact_tag) != "None":
			tmpList.append((self.artifact_tag, 'arti'))
		if tag_header.tagCheck(self.target_detonation_projectile_group_tag) != "None":
			tmpList.append((self.target_detonation_projectile_group_tag, 'prgr'))
		if tag_header.tagCheck(self.geometry_tag) != "None":
			tmpList.append((self.geometry_tag, 'geom'))
		if tag_header.tagCheck(self.local_projectile_group_tag) != "None":
			tmpList.append((self.local_projectile_group_tag, 'lpgr'))
		if tag_header.tagCheck(self.promotion_unit_tag) != "None":
			tmpList.append((self.promotion_unit_tag, 'unit'))
		
		if len(tmpList)>0:
			return tmpList
		else:
			return False