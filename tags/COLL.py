import tag_header,struct

SHAPE_NAME_LENGTH= 63
EXTRACTOR_DATA_SIZE= 16
NUMBER_OF_SEQUENCE_SOUNDS=3
SEQUENCE_REFERENCE_HEADER_LENGTH=128
COLLECTION_HEADER_LENGTH=320
SEQUENCE_DATA_HEADER_LENGTH=64

class sequence_data:
    def __init__(self, dFile):
        self.flags=struct.unpack(">L", dFile.read(4))[0]
        
        self.pixels_to_world=struct.unpack(">l", dFile.read(4))[0]
        
        self.number_of_views=struct.unpack(">h", dFile.read(2))[0]
        self.frames_per_view=struct.unpack(">h", dFile.read(2))[0]
        self.ticks_per_frame=struct.unpack(">h", dFile.read(2))[0]
        self.key_frame=struct.unpack(">h", dFile.read(2))[0] 
        self.loop_frame=struct.unpack(">h", dFile.read(2))[0]
        
        self.sound_indexes=[]
        for i in range(NUMBER_OF_SEQUENCE_SOUNDS):
            self.sound_indexes.append(struct.unpack(">h", dFile.read(2))[0])
            
        self.sound_tags=[]
        for i in range(NUMBER_OF_SEQUENCE_SOUNDS):
            self.sound_tags.append(struct.unpack(">L", dFile.read(4))[0])
        
        self.transfer_mode=struct.unpack(">h", dFile.read(2))[0]
        self.transfer_period=struct.unpack(">h", dFile.read(2))[0]
        
        self.radius=struct.unpack(">l", dFile.read(4))[0]
        self.height0=struct.unpack(">l", dFile.read(4))[0]
        self.height1=struct.unpack(">l", dFile.read(4))[0]    
        self.world_radius=struct.unpack(">h", dFile.read(2))[0] 
        self.world_height0=struct.unpack(">h", dFile.read(2))[0]
        self.world_height1=struct.unpack(">h", dFile.read(2))[0]
    
        self.unused=[]
        for i in range(3):
            self.unused.append(struct.unpack(">h", dFile.read(2))[0])



class sequence_reference:
    def __init__(self, dFile, startingOffset):
        self.name=[]
        for i in range(SHAPE_NAME_LENGTH+1):
            self.name.append(struct.unpack(">c", dFile.read(1))[0])
        self.offset=struct.unpack(">l", dFile.read(4))[0]
        self.size=struct.unpack(">l", dFile.read(4))[0]
        self.sequence_pointer=struct.unpack(">L", dFile.read(4))[0]
        
        currentOffset=dFile.tell()
        self.sequence=[]
        
        for i in range(self.size/SEQUENCE_DATA_HEADER_LENGTH):
        #print "     " + "".join(self.name) + " : " + str(self.size/SEQUENCE_DATA_HEADER_LENGTH) + " : " + str(startingOffset + self.offset + COLLECTION_HEADER_LENGTH + (SEQUENCE_DATA_HEADER_LENGTH * i))
            dFile.seek(startingOffset + self.offset + COLLECTION_HEADER_LENGTH + (SEQUENCE_DATA_HEADER_LENGTH * i))
            self.sequence.append(sequence_data(dFile))
        dFile.seek(currentOffset)
        
        self.unused=[]
        for i in range(18):
            self.unused.append(struct.unpack(">h", dFile.read(2))[0])
        self.extractor_data=[]
        for i in range(EXTRACTOR_DATA_SIZE):
            self.extractor_data.append(struct.unpack(">c", dFile.read(1))[0])

class COLL:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        
    def loadTag(self, dFile):
        if self.isLocal:
            dFile.seek(64)
        else:
            dFile.seek(self.tagHeader.getOffset())
        self.flags=struct.unpack(">L", dFile.read(4))[0]
        
        self.user_data_flags=struct.unpack(">L", dFile.read(4))[0]
        self.unused1=[]
        for i in range(28):
            self.unused1.append(struct.unpack(">h", dFile.read(2))[0])
        
        self.color_table_count=struct.unpack(">l", dFile.read(4))[0] 
        self.color_tables_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.color_tables_size=struct.unpack(">l", dFile.read(4))[0]
        self.color_tables=struct.unpack(">L", dFile.read(4))[0]
        
        self.hue_change_count=struct.unpack(">L", dFile.read(4))[0]
        self.hue_changes_offset=struct.unpack(">L", dFile.read(4))[0] 
        self.hue_changes_size=struct.unpack(">L", dFile.read(4))[0]
        self.hue_changes=struct.unpack(">L", dFile.read(4))[0]
        
        self.bitmap_count=struct.unpack(">l", dFile.read(4))[0]
        self.bitmap_references_offset=struct.unpack(">l", dFile.read(4))[0]
        self.bitmap_references_size=struct.unpack(">l", dFile.read(4))[0]
        self.bitmap_references=struct.unpack(">L", dFile.read(4))[0]
        
        self.bitmap_instance_count=struct.unpack(">l", dFile.read(4))[0]
        self.bitmap_instances_offset=struct.unpack(">l", dFile.read(4))[0]
        self.bitmap_instances_size=struct.unpack(">l", dFile.read(4))[0]
        self.bitmap_instances=struct.unpack(">L", dFile.read(4))[0]
        
        self.sequence_count=struct.unpack(">l", dFile.read(4))[0]
        self.sequence_references_offset=struct.unpack(">l", dFile.read(4))[0]
        self.sequence_references_size=struct.unpack(">l", dFile.read(4))[0]
        self.sequence_references_pointer=struct.unpack(">L", dFile.read(4))[0]
        currentOffset=dFile.tell()
        self.sequence_references=[]
        #print self.tagHeader.name + " : " + str(self.sequence_references_size) + " : " + str(self.tagHeader.getOffset() + self.sequence_references_offset + 320)
        for i in range(self.sequence_references_size/SEQUENCE_REFERENCE_HEADER_LENGTH):
            dFile.seek(self.tagHeader.getOffset() + self.sequence_references_offset + COLLECTION_HEADER_LENGTH + (SEQUENCE_REFERENCE_HEADER_LENGTH * i))
            self.sequence_references.append(sequence_reference(dFile, self.tagHeader.getOffset()))
        dFile.seek(currentOffset)
        
        self.shadow_map_count=struct.unpack(">l", dFile.read(4))[0]
        self.shadow_maps_offset=struct.unpack(">l", dFile.read(4))[0]
        self.shadow_maps_size=struct.unpack(">l", dFile.read(4))[0]
        self.shadow_maps=struct.unpack(">L", dFile.read(4))[0]
        
        self.blend_table_count=struct.unpack(">l", dFile.read(4))[0]
        self.blend_table_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.blend_table_size=struct.unpack(">l", dFile.read(4))[0]
        self.blend_table=struct.unpack(">L", dFile.read(4))[0]
        
        self.remapping_table_count=struct.unpack(">l", dFile.read(4))[0]
        self.remapping_table_offset=struct.unpack(">l", dFile.read(4))[0]
        self.remapping_table_size=struct.unpack(">l", dFile.read(4))[0]
        self.remapping_table=struct.unpack(">L", dFile.read(4))[0]
        
        self.shading_table_count=struct.unpack(">l", dFile.read(4))[0]
        self.shading_table_pointers=struct.unpack(">L", dFile.read(4))[0]
        
        self.unused3=[]
        for i in range(24):
            self.unused3.append(struct.unpack(">h", dFile.read(2))[0])
        
        self.data_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.data_size=struct.unpack(">l", dFile.read(4))[0] 
        self.data_delta=struct.unpack(">l", dFile.read(4))[0]
        self.data=struct.unpack(">L", dFile.read(4))[0]
        
        self.unused2=[]
        for i in range(20):
            self.unused2.append(struct.unpack(">h", dFile.read(2))[0])
        
        self.extractor_data=[]
        for i in range(EXTRACTOR_DATA_SIZE):
            self.extractor_data.append(struct.unpack(">c", dFile.read(1))[0])
                
    def writeCollection(self, dFile, dOffset):
        pass
        #If we ever really write out the whole collection that code would go here.
        #self.tagHeader.writeHeader(dFile, dOffset)
        #dFile.seek(64)
        #dFile.write(struct.pack(">L", self.flags))
        #dFile.write(struct.pack(">h", self.Identifier))
        
    def getLinkedTags(self):
        tmpList=[]
        tagFile = file(self.fPath, "rb")
        self.loadTag(tagFile)
        for seqRef in self.sequence_references:
            for seq in seqRef.sequence:    
                for dTag in seq.sound_tags:
                    if tag_header.tagCheck(dTag) != "None":
                        tmpList.append((dTag, 'soun'))
        if len(tmpList)>0:
            return tmpList
        else:
            return False
    def get_SHAPE_NAME_LENGTH(self):
        return SHAPE_NAME_LENGTH
    
    def get_EXTRACTOR_DATA_SIZE(self):
        return EXTRACTOR_DATA_SIZE
    
    def get_NUMBER_OF_SEQUENCE_SOUNDS(self): 
        return NUMBER_OF_SEQUENCE_SOUNDS       
    
    def get_SEQUENCE_REFERENCE_HEADER_LENGTH(self):
        return SEQUENCE_REFERENCE_HEADER_LENGTH    
    
    def get_COLLECTION_HEADER_LENGTH(self):
        return COLLECTION_HEADER_LENGTH    
    
    def get_SEQUENCE_DATA_HEADER_LENGTH(self):
        return SEQUENCE_DATA_HEADER_LENGTH
    
