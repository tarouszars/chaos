import tag_header,struct

class rgb_color:
	def __init__(self, dFile):
		self.red=struct.unpack(">h", dFile.read(2))[0]
		self.green=struct.unpack(">h", dFile.read(2))[0]
		self.blue=struct.unpack(">h", dFile.read(2))[0]
		self.flags=struct.unpack(">h", dFile.read(2))[0]

class CREF:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE=5
		self.MAXIMUM_HUE_CHANGES_PER_COLLECTION=8
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			dFile.seek(self.tagHeader.getOffset())
			self.isLocal=False
		self.collection_tag=struct.unpack(">L", dFile.read(4))[0] 
		
		self.number_of_permutations=struct.unpack(">h", dFile.read(2))[0]
		self.tint_fraction=struct.unpack(">h", dFile.read(2))[0]
		self.colors=[]
		for i in range(self.MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE):
			tmpList=[]
			for i in range(self.MAXIMUM_HUE_CHANGES_PER_COLLECTION):
				tmpList.append(rgb_color(dFile))
			self.colors.append(tmpList)
		self.tint_color=rgb_color(dFile)
		
		unused=[]
		for i in range(18):
			unused.append(struct.unpack(">h", dFile.read(2))[0])
		
		self.collection_index=struct.unpack(">h", dFile.read(2))[0]
		self.color_table_indexes=[]
		for i in range(self.MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE):
			self.color_table_indexes.append(struct.unpack(">h", dFile.read(2))[0])

		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.collection_tag) != "None":
			tmpList.append((self.collection_tag, '.256'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False