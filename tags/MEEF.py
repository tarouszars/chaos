import tag_header,struct

class MEEF:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			self.isLocal=False
			dFile.seek(self.tagHeader.getOffset())
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		self.amplitude=struct.unpack(">h", dFile.read(2))[0]
		
		self.cutoff_radius=struct.unpack(">h", dFile.read(2))[0]
		self.dissipation=struct.unpack(">h", dFile.read(2))[0]
		
		self.velocity=struct.unpack(">h", dFile.read(2))[0]
		self.epicenter_offset=struct.unpack(">h", dFile.read(2))[0]
		
		self.camera_shaking_type=struct.unpack(">h", dFile.read(2))[0]
		
		self.collection_reference_tag=struct.unpack(">L", dFile.read(4))[0]
		self.sequence_index=struct.unpack(">h", dFile.read(2))[0]
		self.number_of_radial_points=struct.unpack(">h", dFile.read(2))[0]
		
		self.shockwave_thickness=struct.unpack(">h", dFile.read(2))[0]
		self.shockwave_height_offset=struct.unpack(">h", dFile.read(2))[0]
		
		## computed during postprocessing
		
		self.collection_reference_index=struct.unpack(">h", dFile.read(2))[0]
		self.collection_index=struct.unpack(">h", dFile.read(2))[0]
		self.color_table_index=struct.unpack(">h", dFile.read(2))[0]
		self.pad=struct.unpack(">h", dFile.read(2))[0]
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.collection_reference_tag) != "None":
			tmpList.append((self.collection_reference_tag, 'core'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False