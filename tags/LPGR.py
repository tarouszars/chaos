import tag_header,struct

#46288
class LPGR:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		MAXIMUM_NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES= 2
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			dFile.seek(64)
			self.isLocal=True
		else:
			dFile.seek(self.tagHeader.getOffset())
			self.isLocal=False
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		self.collection_reference_tag=struct.unpack(">L", dFile.read(4))[0] 	             	##TAG
		self.physics_tag=struct.unpack(">L", dFile.read(4))[0]                    				##TAG
		self.sequence_indexes=[]
		for i in range(MAXIMUM_NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES):
			self.sequence_indexes.append(struct.unpack(">h", dFile.read(2))[0])
		self.scale_lower_bound = struct.unpack(">h", dFile.read(2))[0]
		self.scale_delta = struct.unpack(">h", dFile.read(2))[0]
		self.animation_rate_lower_bound = struct.unpack(">h", dFile.read(2))[0]
		self.animation_rate_delta = struct.unpack(">h", dFile.read(2))[0]
		self.target_number_lower_bound = struct.unpack(">h", dFile.read(2))[0]
		self.target_number_delta = struct.unpack(">h", dFile.read(2))[0]
		self.radius_lower_bound=struct.unpack(">l", dFile.read(4))[0] 
		self.radius_delta=struct.unpack(">l", dFile.read(4))[0] 
		self.expiration_time_lower_bound=struct.unpack(">i", dFile.read(4))[0] 
		self.expiration_time_delta=struct.unpack(">i", dFile.read(4))[0] 
		self.transfer_mode = struct.unpack(">h", dFile.read(2))[0]
		self.height_offset = struct.unpack(">h", dFile.read(2))[0]
		self.startup_transition_time_lower_bound = struct.unpack(">h", dFile.read(2))[0]
		self.startup_transition_time_delta = struct.unpack(">h", dFile.read(2))[0]
		self.ending_transition_time_lower_bound = struct.unpack(">h", dFile.read(2))[0]
		self.ending_transition_time_delta = struct.unpack(">h", dFile.read(2))[0]
		self.duration_lower_bound=struct.unpack(">i", dFile.read(4))[0] 
		self.duration_delta=struct.unpack(">i", dFile.read(4))[0] 
		self.initial_z_velocity = struct.unpack(">h", dFile.read(2))[0]
		self.z_acceleration = struct.unpack(">h", dFile.read(2))[0]
		self.startup_delay_lower_bound = struct.unpack(">h", dFile.read(2))[0]
		self.startup_delay_delta = struct.unpack(">h", dFile.read(2))[0]
		self.unused2=dFile.read(8)
		self.chain_to_lpgr=struct.unpack(">L", dFile.read(4))[0]                     				##TAG
		self.local_light_tag=struct.unpack(">L", dFile.read(4))[0]                     				##TAG
		self.unused=dFile.read(28)
		
		#// computed during postprocessing
		self.collection_reference_index = struct.unpack(">h", dFile.read(2))[0]
		self.collection_index = struct.unpack(">h", dFile.read(2))[0]
		self.color_table_index = struct.unpack(">h", dFile.read(2))[0]
		self.physics_index = struct.unpack(">h", dFile.read(2))[0]
		self.chain_to_lpgr_index = struct.unpack(">h", dFile.read(2))[0]
		self.local_light_index = struct.unpack(">h", dFile.read(2))[0]
		
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.collection_reference_tag) != "None":
			tmpList.append((self.collection_reference_tag, 'core'))
		if tag_header.tagCheck(self.physics_tag) != "None":
			tmpList.append((self.physics_tag, 'phys'))
		if tag_header.tagCheck(self.chain_to_lpgr) != "None":
			tmpList.append((self.chain_to_lpgr, 'lpgr'))
		if tag_header.tagCheck(self.local_light_tag) != "None":
			tmpList.append((self.local_light_tag, 'ligh'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False
		
		