if __name__ == "__main__":
    import sys, pdb
    sys.path.append('../')
    DEBUG = True
else:
    DEBUG = False

import tag_header,struct
# class Markers:
marker_types = [
    None, #"_marker_team", #no tag 
    "scen",
    None, #"_marker_unused1",
    "unit", # refers to unit tag
    None, #"_marker_unused2",
    "amso",
    "mode",
    None, #"_marker_unused3",
    None, #"_marker_unused4",
    "proj",
    "lpgr",
    "anim"
]
NUMBER_OF_MARKER_TYPES = len(marker_types)
SIZEOF_STRUCT_MAP_ACTION = 64
SIZEOF_MAP_ACTION_PARAMETER_HEADER = 8

parameter_definitions=[1,1,2,2,2,4,4,4,4,4,4,4,4,8,16,2,2,2,12,2,2]
def get_aligned_parameter_size(type, count):
    size = count*parameter_definitions[type]
    if (size%4):
        size = int(size-(size%4))+4
    return size;


class marker_palette_entry:
    SIZEOF_STRUCT_MARKER_PALETTE_ENTRY = 32
    def __init__(self, dFile):
        self.type = struct.unpack(">h", dFile.read(2))[0]
        self.flags = struct.unpack(">H", dFile.read(2))[0]
        self.tag = struct.unpack(">L", dFile.read(4))[0]
        self.team_index = struct.unpack(">h", dFile.read(2))[0]
        self.pad = struct.unpack(">h", dFile.read(2))[0]
        self.extra_flags = struct.unpack(">L", dFile.read(4))[0]
        self.unused = dFile.read(12)
        self.count = struct.unpack(">h", dFile.read(2))[0]
        self.tag_index = struct.unpack(">h", dFile.read(2))[0]
    

class marker_data:
    SIZEOF_MARKER_USER_DATA = 16
    SIZEOF_STRUCT_MARKER_DATA = 64
    def __init__(self, dFile):
        self.flags = struct.unpack(">L", dFile.read(4))[0]
        self.type = struct.unpack(">h", dFile.read(2))[0]
        self.palette_index = struct.unpack(">h", dFile.read(2))[0]
        self.identifier = struct.unpack(">h", dFile.read(2))[0]
        self.minimum_difficulty_level = struct.unpack(">h", dFile.read(2))[0]
        self.location = dFile.read(24)
        self.user_data = dFile.read(self.SIZEOF_MARKER_USER_DATA)
        self.unused = dFile.read(8)
        self.data_index = struct.unpack(">h", dFile.read(2))[0]
        self.data_identifier = struct.unpack(">h", dFile.read(2))[0]
    

class map_action_parameter:
    def __init__(self, dFile):
        self.type = struct.unpack(">h", dFile.read(2))[0]
        self.count = struct.unpack(">h", dFile.read(2))[0]
        self.field_name = dFile.read(4)
    

class map_action:
    def __init__(self, dFile):
        stuff1=dFile.read(4)
        self.tag = dFile.read(4)#struct.unpack(">L", dFile.read(4))[0]
        stuff2 = dFile.read(12)
        self.parameter_count=struct.unpack(">h", dFile.read(2))[0]
        self.parameters_size=struct.unpack(">h", dFile.read(2))[0]
        self.parameters_offset=(struct.unpack(">l", dFile.read(4))[0])
        self.visual_indent = struct.unpack(">h", dFile.read(2))[0]
        unused=dFile.read(30)
        self.trigger_time=struct.unpack(">l", dFile.read(4))[0]
    

class MESH:
    NUMBER_OF_NEXT_MESH_TAGS=2
    NUMBER_OF_CUTSCENE_MOVIE_TAGS=3
    NUMBER_OF_STORYLINE_STRING_TAGS=4
    NUMBER_OF_SCREEN_COLLECTION_TAGS=3
    SIZEOF_STRUCT_MESH_HEADER = 1024
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        self.landscape_collection_tag=struct.unpack(">L", dFile.read(4))[0]
        self.media_tag=struct.unpack(">L", dFile.read(4))[0]
        
        self.submesh_width=struct.unpack(">h", dFile.read(2))[0] 
        self.submesh_height=struct.unpack(">h", dFile.read(2))[0]
        self.mesh_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.mesh_size=struct.unpack(">l", dFile.read(4))[0]
        self.mesh=struct.unpack(">l", dFile.read(4))[0]
     
        self.data_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.data_size=struct.unpack(">l", dFile.read(4))[0]
        self.data=struct.unpack(">l", dFile.read(4))[0]
         
        self.marker_palette_entries=struct.unpack(">l", dFile.read(4))[0] 
        self.marker_palette_offset=struct.unpack(">l", dFile.read(4))[0]  
        self.marker_palette_size=struct.unpack(">l", dFile.read(4))[0] 
        self.marker_palette=struct.unpack(">l", dFile.read(4))[0] 
        self.marker_palette_list = []

        self.marker_count=struct.unpack(">l", dFile.read(4))[0] 
        self.markers_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.markers_size=struct.unpack(">l", dFile.read(4))[0]
        self.markers=struct.unpack(">l", dFile.read(4))[0]
     
        self.mesh_lighting_tag=struct.unpack(">L", dFile.read(4))[0] # shadow intensity, ambient light, etc.
        self.connector_tag=struct.unpack(">L", dFile.read(4))[0] # for fences
        self.flags=struct.unpack(">L", dFile.read(4))[0]
        self.particle_system_tag=struct.unpack(">L", dFile.read(4))[0]
        self.team_count=struct.unpack(">l", dFile.read(4))[0]
        #     
        self.dark_fraction=struct.unpack(">h", dFile.read(2))[0]
        self.light_fraction=struct.unpack(">h", dFile.read(2))[0]
        self.dark_color=dFile.read(8) #     struct rgb_color dark_color;
        self.light_color=dFile.read(8) #     struct rgb_color light_color;
        self.transition_point=struct.unpack(">L", dFile.read(4))[0]
        # 
        self.ceiling_height=struct.unpack(">h", dFile.read(2))[0]
        self.unused1=struct.unpack(">h", dFile.read(2))[0]
        # 
        #     // this is not a rectangle; these are the buffers on each edge of the map, in cells
        self.edge_of_mesh_buffer_zones=dFile.read(8) #     rectangle2d edge_of_mesh_buffer_zones;
        #     
        self.global_ambient_sound_tag=struct.unpack(">L", dFile.read(4))[0]
        # 
        self.map_action_count=struct.unpack(">l", dFile.read(4))[0]
        self.map_actions_offset=struct.unpack(">l", dFile.read(4))[0] 
        self.map_action_buffer_size=struct.unpack(">l", dFile.read(4))[0]
        # 
        self.map_description_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
        self.postgame_collection_tag=struct.unpack(">L", dFile.read(4))[0]
        self.pregame_collection_tag=struct.unpack(">L", dFile.read(4))[0]
        self.overhead_map_collection_tag=struct.unpack(">L", dFile.read(4))[0]
        self.next_mesh_tags=[]
        for i in range(self.NUMBER_OF_NEXT_MESH_TAGS):
            self.next_mesh_tags.append(struct.unpack(">L", dFile.read(4))[0])
        self.cutscene_movie_tags=[]
        for i in range(self.NUMBER_OF_CUTSCENE_MOVIE_TAGS):
            self.cutscene_movie_tags.append(struct.unpack(">L", dFile.read(4))[0])
        self.storyline_string_tags=[]
        for i in range(self.NUMBER_OF_STORYLINE_STRING_TAGS):
            self.storyline_string_tags.append(struct.unpack(">L", dFile.read(4))[0])
        self.media_coverage_region_offset=struct.unpack(">L", dFile.read(4))[0]
        #     
        self.media_coverage_region_size=struct.unpack(">L", dFile.read(4))[0]
        self.media_coverage_region=struct.unpack(">h", dFile.read(2))[0]
        #     
        self.mesh_LOD_data_offset=struct.unpack(">L", dFile.read(4))[0]
        self.mesh_LOD_data_size=struct.unpack(">L", dFile.read(4))[0]
        self.mesh_LOD_data=struct.unpack(">L", dFile.read(4))[0]
        # 
        self.global_tint_color=dFile.read(8) #     struct rgb_color global_tint_color;
        self.global_tint_fraction=struct.unpack(">h", dFile.read(2))[0]
        self.pad=struct.unpack(">L", dFile.read(4))[0]
        #     
        self.wind_tag=struct.unpack(">L", dFile.read(4))[0]
        self.screen_collection_tags=[]
        for i in range(self.NUMBER_OF_SCREEN_COLLECTION_TAGS):
            self.screen_collection_tags.append(struct.unpack(">L", dFile.read(4))[0])
        #     
        self.blood_color=dFile.read(8) #     struct rgb_color blood_color;
        # 
        self.picture_caption_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
        self.narration_sound_tag=struct.unpack(">L", dFile.read(4))[0]
        self.win_ambient_sound_tag=struct.unpack(">L", dFile.read(4))[0]
        self.loss_ambient_sound_tag=struct.unpack(">L", dFile.read(4))[0]
        # 
        #   For Creative's Environmental Audio Extensions
        self.reverb_environment=struct.unpack(">L", dFile.read(4))[0] 
        self.reverb_volume=struct.unpack(">f", dFile.read(4))[0]
        self.reverb_decay_time=struct.unpack(">f", dFile.read(4))[0]
        self.reverb_damping=struct.unpack(">f", dFile.read(4))[0]
        
        #   array of connectors (i.e. fences)
        self.connector_count=struct.unpack(">l", dFile.read(4))[0] 
        self.connectors_offset=struct.unpack(">l", dFile.read(4))[0]  
        self.connectors_size=struct.unpack(">l", dFile.read(4))[0] 
        self.connectors=struct.unpack(">l", dFile.read(4))[0]  #What is this?     struct saved_connector_data *connectors;
     
        self.cutscene_names=[]
        for i in range(self.NUMBER_OF_CUTSCENE_MOVIE_TAGS):
            self.cutscene_names.append(dFile.read(64))
     
        self.hints_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
        # 
        self.fog_color=dFile.read(8) #     struct rgb_color fog_color;
        self.fog_density=struct.unpack(">f", dFile.read(4))[0]
        
        self.difficulty_level_override_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
        self.team_names_override_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
     
        self.plugin_name=dFile.read(32)
        self.extra_flags=struct.unpack(">L", dFile.read(4))[0]
        self.unused=dFile.read(442)
        
        self.connector_type=struct.unpack(">h", dFile.read(2))[0]
        self.map_description_string_index=struct.unpack(">h", dFile.read(2))[0]
        self.overhead_map_collection_index=struct.unpack(">h", dFile.read(2))[0]
        self.landscape_collection_index=struct.unpack(">h", dFile.read(2))[0]
        self.global_ambient_sound_index=struct.unpack(">h", dFile.read(2))[0]
        self.media_type=struct.unpack(">h", dFile.read(2))[0]
        self.hints_string_list_index=struct.unpack(">h", dFile.read(2))[0]
         
        #     byte editor_data[MESH_HEADER_EDITOR_DATA_SIZE];


    def getTagName(self):
        return self.tagHeader.name
    
    def getLinkedTags(self):
        tmpList=[]
        #the dmap will have the same tag as self
        tmpList.append((tag_header.prepareString(self.tagHeader.subgroup_tag), 'dmap'))
        if tag_header.tagCheck(self.landscape_collection_tag) != "None":
            tmpList.append((self.landscape_collection_tag, '.256'))
        if tag_header.tagCheck(self.media_tag) != "None":
            tmpList.append((self.media_tag, 'medi'))
        if tag_header.tagCheck(self.mesh_lighting_tag) != "None":
            tmpList.append((self.mesh_lighting_tag, 'meli'))
        if tag_header.tagCheck(self.connector_tag) != "None":
            tmpList.append((self.connector_tag, 'conn'))
        if tag_header.tagCheck(self.particle_system_tag) != "None":
            tmpList.append((self.particle_system_tag, 'part'))
        if tag_header.tagCheck(self.map_description_string_list_tag) != "None":
            tmpList.append((self.map_description_string_list_tag, 'stli'))
        if tag_header.tagCheck(self.postgame_collection_tag) != "None":
            tmpList.append((self.postgame_collection_tag, '.256'))
        if tag_header.tagCheck(self.pregame_collection_tag) != "None":
            tmpList.append((self.pregame_collection_tag, '.256'))
        if tag_header.tagCheck(self.overhead_map_collection_tag) != "None":
            tmpList.append((self.overhead_map_collection_tag, '.256'))
        # Don't extract the next mesh noob!
        #for i in range(self.NUMBER_OF_NEXT_MESH_TAGS):    
        #    if tag_header.tagCheck(self.next_mesh_tags[i]) != "None":
        #        tmpList.append((self.next_mesh_tags[i], 'mesh'))
        for i in range(self.NUMBER_OF_CUTSCENE_MOVIE_TAGS):
            if tag_header.tagCheck(self.cutscene_movie_tags[i]) != "None":
                tmpList.append((self.cutscene_movie_tags[i], 'cuts'))
        for i in range(self.NUMBER_OF_STORYLINE_STRING_TAGS):
            if tag_header.tagCheck(self.storyline_string_tags[i]) != "None":
                tmpList.append((self.storyline_string_tags[i], 'text'))
        if tag_header.tagCheck(self.wind_tag) != "None":
            tmpList.append((self.wind_tag, 'wind'))
        for i in range(self.NUMBER_OF_SCREEN_COLLECTION_TAGS):
            if tag_header.tagCheck(self.screen_collection_tags[i]) != "None":
                tmpList.append((self.screen_collection_tags[i], '.256'))
        if tag_header.tagCheck(self.picture_caption_string_list_tag) != "None":
            tmpList.append((self.picture_caption_string_list_tag, 'stli'))
        if tag_header.tagCheck(self.narration_sound_tag) != "None":
            tmpList.append((self.narration_sound_tag, 'soun'))
        if tag_header.tagCheck(self.win_ambient_sound_tag) != "None":
            tmpList.append((self.win_ambient_sound_tag, 'soun'))
        if tag_header.tagCheck(self.loss_ambient_sound_tag) != "None":
            tmpList.append((self.loss_ambient_sound_tag, 'soun'))
        if tag_header.tagCheck(self.hints_string_list_tag) != "None":
            tmpList.append((self.hints_string_list_tag, 'stli'))
        if tag_header.tagCheck(self.difficulty_level_override_string_list_tag) != "None":
            tmpList.append((self.difficulty_level_override_string_list_tag, 'stli'))
        if tag_header.tagCheck(self.team_names_override_string_list_tag) != "None":
            tmpList.append((self.team_names_override_string_list_tag, 'stli'))
            
        #find markers
        if DEBUG: print "\tself.marker_palette_offset: " + str(self.marker_palette_offset ) + "\n\tself.data_offset: " + str(self.data_offset) + "\n\tself.tagHeader.getOffset(): " + str(self.tagHeader.getOffset())
        the_file = open(self.fPath, 'rb')
        the_file.seek(self.marker_palette_offset+self.data_offset + self.tagHeader.getOffset())
        for i in range(self.marker_palette_entries):
            temp_marker_palette_entry = marker_palette_entry(the_file)
            if (temp_marker_palette_entry.type >0 and temp_marker_palette_entry.type <NUMBER_OF_MARKER_TYPES):
                if (marker_types[temp_marker_palette_entry.type]):
                    if tag_header.tagCheck(temp_marker_palette_entry.tag) != "None":
                        tmpList.append((temp_marker_palette_entry.tag, marker_types[temp_marker_palette_entry.type]))
                        if DEBUG: print tmpList[-1]
        
        #get what you need out of the map actions here
        action_offset = self.data_offset+self.map_actions_offset + self.tagHeader.getOffset()
        map_actions_size = ((self.map_action_count)*(SIZEOF_STRUCT_MAP_ACTION)) # We have to plus 1 because we want to be AFTER the last one
        param_offset = action_offset + map_actions_size
        # Find particular map actions in the buffer
        the_file.seek(action_offset)
        for i in range(self.map_action_count):
            m_action = map_action(the_file)
            pos = the_file.tell()
            if (m_action.tag=="soun"):
                param_location = param_offset + m_action.parameters_offset
                for j in range(m_action.parameter_count):
                    the_file.seek(param_location)
                    map_param = map_action_parameter(the_file)
                    if (map_param.field_name=="soun"):
                        tag_name = struct.unpack(">L", the_file.read(4))[0]
                        if tag_header.tagCheck(tag_name) != "None":
                            tmpList.append((tag_name, 'soun'))
                            if DEBUG: print tmpList[-1]
                    param_location+=(SIZEOF_MAP_ACTION_PARAMETER_HEADER + get_aligned_parameter_size(map_param.type, map_param.count))
                the_file.seek(pos)
        the_file.close()
        return tmpList
    

if __name__ == "__main__":
    filepath = "../dist/local/meshes/407 Hedgerows-mesh"
    d = file(filepath, 'rb')
    f = MESH(d, 0, 1, filepath)
    linked = f.getLinkedTags()
    for tag, _type in linked:
        print _type + ":" + tag_header.tagCheck(tag)