import tag_header,struct
from tags.MONS import monster_attack_definition

class ARTI:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES = 4
        self.MAXIMUM_NUMBER_OF_ARTIFACT_SEQUENCES = 4
        self.MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS = 16
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            self.isLocal=False
            dFile.seek(self.tagHeader.getOffset())
        self.flags=struct.unpack(">L", dFile.read(4))[0]
        self.monster_restriction_tag=struct.unpack(">L", dFile.read(4))[0]
        self.specialization_restriction=struct.unpack(">h", dFile.read(2))[0]
        self.initial_charges_lower_bound=struct.unpack(">h", dFile.read(2))[0]
        self.initial_charges_delta=struct.unpack(">h", dFile.read(2))[0]
        self.pad=struct.unpack(">h", dFile.read(2))[0]
        self.collection_tag=struct.unpack(">L", dFile.read(4))[0]
        self.sequence_indexes=[]
        for i in range(self.MAXIMUM_NUMBER_OF_ARTIFACT_SEQUENCES):
            self.sequence_indexes.append(struct.unpack(">h", dFile.read(2))[0])
        self.projectile_tags=[]
        for i in range(self.MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES):
            self.projectile_tags.append(struct.unpack(">L", dFile.read(4))[0])
        self.bonus_monster_flags=struct.unpack(">L", dFile.read(4))[0]
        self.monster_override_type=struct.unpack(">h", dFile.read(2))[0]
        self.expiry_timer=struct.unpack(">h", dFile.read(2))[0]
        self.bonus_effect_modifiers=[]
        for i in range(self.MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS):
            self.bonus_effect_modifiers.append(struct.unpack(">h", dFile.read(2))[0])
        self.override_attack=monster_attack_definition(dFile)
        self.special_ability_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
        self.monster_override_tag=struct.unpack(">L", dFile.read(4))[0]
        self.collection_index=struct.unpack(">h", dFile.read(2))[0]
        self.special_ability_string_list_index=struct.unpack(">h", dFile.read(2))[0]
        self.projectile_types=[]
        for i in range(self.MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES):
            self.projectile_types.append(struct.unpack(">h", dFile.read(2))[0])
            
    def getLinkedTags(self):
        tmpList=[]
        if tag_header.tagCheck(self.monster_restriction_tag) != "None":
            tmpList.append((self.monster_restriction_tag, 'mons'))
        if tag_header.tagCheck(self.collection_tag) != "None":
            tmpList.append((self.collection_tag, '.256'))
        for dProj in self.projectile_tags:
            if tag_header.tagCheck(dProj) != "None":
                tmpList.append((dProj, 'proj'))
        if tag_header.tagCheck(self.override_attack.projectile_tag) != "None":
            tmpList.append((self.override_attack.projectile_tag, 'proj'))
        if tag_header.tagCheck(self.special_ability_string_list_tag) != "None":
            tmpList.append((self.special_ability_string_list_tag, 'stli'))
        if tag_header.tagCheck(self.monster_override_tag) != "None":
            tmpList.append((self.monster_override_tag, 'mons'))
        if len(tmpList)>0:
            return tmpList
        else:
            return False
    def getTagName(self):
        return self.tagHeader.name