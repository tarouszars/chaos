import tag_header,struct



class projectile_group_part_definition:
	def __init__(self,dFile,dOffset):
		dFile.seek(dOffset)
		self.projectile_tag=struct.unpack(">L", dFile.read(4))[0] #dFile.read(4)
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		self.count_lower_bound=struct.unpack(">h", dFile.read(2))[0]
		self.count_delta=struct.unpack(">h", dFile.read(2))[0]
		self.position_lower_bound=struct.unpack(">h", dFile.read(2))[0]
		self.position_delta=struct.unpack(">h", dFile.read(2))[0]
		unused=dFile.read(4)
		self.appearing_fraction=struct.unpack(">H", dFile.read(2))[0]
		unused=dFile.read(2)
		self.alternative_projectile_tag=struct.unpack(">L", dFile.read(4))[0]
		self.alternative_projectile_type=struct.unpack(">h", dFile.read(2))[0]
		self.projectile_type=struct.unpack(">h", dFile.read(2))[0]
	def getPROJCode(self): 
		return tag_header.tagCheck(self.projectile_tag)
	
	def getCountUpperBound(self): return self.count_lower_bound+self.count_delta
	def getPositionUpperBound(self): return round((self.position_lower_bound/256.0)+(self.position_delta/256.0), 3)
	def getPositionLowerBound(self): return round((self.position_lower_bound/256.0), 3)
	def getAppearingFraction(self): return round((self.appearing_fraction/65535.000),3)
	def getIsOnGround(self):
		if self.flags/2>0:
			return "yes"
		else:
			return "no"
	def getIsCentral(self):
		if self.flags/2>0:
			tmpFlags=self.flags%2
		else:
			tmpFlags=self.flags
		if tmpFlags==1:
			return "yes"
		else:
			return "no"
	
class PRGR:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			self.isLocal=False
			dFile.seek(self.tagHeader.getOffset())
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		self.number_of_parts=struct.unpack(">H", dFile.read(2))[0]
		self.pad=struct.unpack(">H", dFile.read(2))[0]
		self.mesh_effect_tag=struct.unpack(">L", dFile.read(4))[0]
		self.sound_tag=struct.unpack(">L", dFile.read(4))[0]
		self.local_projectile_group_tag=struct.unpack(">L", dFile.read(4))[0]
		unused=dFile.read(6)
		self.local_projectile_group_type=struct.unpack(">H", dFile.read(2))[0]
		self.mesh_effect_type=struct.unpack(">H", dFile.read(2))[0]
		self.sound_index=struct.unpack(">H", dFile.read(2))[0]
		self.projList=[]
		myOffset=dFile.tell()
		for i in range(self.number_of_parts):
			aProjDef=projectile_group_part_definition(dFile,myOffset)
			self.projList.append(aProjDef)
			myOffset=myOffset+32
	def getPRGRName(self): return self.tagHeader.getName()
	def getPRGRCode(self): return self.tagHeader.getSubgroup_Tag()
	def getPRGRGroup(self): return self.tagHeader.getGroup_Tag()
	def getIsLocal(self): return self.isLocal
	def getPROJList(self): return self.projList
		
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.mesh_effect_tag) != "None":
			tmpList.append((self.mesh_effect_tag, 'meef'))
		if tag_header.tagCheck(self.sound_tag) != "None":
			tmpList.append((self.sound_tag, 'soun'))
		if tag_header.tagCheck(self.local_projectile_group_tag) != "None":
			tmpList.append((self.local_projectile_group_tag, 'lpgr'))
		for dProj in self.projList:
			if tag_header.tagCheck(dProj.projectile_tag) != "None":
				tmpList.append((dProj.projectile_tag, 'proj'))
			if tag_header.tagCheck(dProj.alternative_projectile_tag) != "None":
				tmpList.append((dProj.alternative_projectile_tag, 'proj'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False