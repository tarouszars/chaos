import tag_header,struct

class SOUN:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		if localFile>0:
			self.isLocal=True
		else:
			self.isLocal=False
			dFile.seek(self.tagHeader.getOffset())
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		self.loudness=struct.unpack(">h", dFile.read(2))[0]
		self.play_fraction=struct.unpack(">H", dFile.read(2))[0]
		self.external_frequency_modifier=struct.unpack(">h", dFile.read(2))[0]
		self.pitch_lower_bound=struct.unpack(">h", dFile.read(2))[0]
		self.pitch_delta=struct.unpack(">h", dFile.read(2))[0]
		self.volume_lower_bound=struct.unpack(">h", dFile.read(2))[0]
		self.volume_delta=struct.unpack(">h", dFile.read(2))[0]
		 
		self.first_subtitle_within_string_list_index=struct.unpack(">h", dFile.read(2))[0]
		
		self.sound_offset=struct.unpack(">l", dFile.read(4))[0]
		self.sound_size=struct.unpack(">l", dFile.read(4))[0]
		
		self.subtitle_string_list_tag=struct.unpack(">L", dFile.read(4))[0]
		
		self.subtitle_string_list_index=struct.unpack(">h", dFile.read(2))[0]
		self.unused=struct.unpack(">H", dFile.read(2))[0]
		
		self.number_of_permutations=struct.unpack(">l", dFile.read(4))[0]
		self.permutations_offset=struct.unpack(">l", dFile.read(4))[0]
		self.permutations_size=struct.unpack(">l", dFile.read(4))[0]
		self.sound_permutations=struct.unpack(">L", dFile.read(4))[0]       ##This bad boy is a poiner
		
		self.reference_count=struct.unpack(">h", dFile.read(2))[0]
		self.permutations_played_flags=struct.unpack(">h", dFile.read(2))[0]
		self.local_tick_last_played=struct.unpack(">L", dFile.read(4))[0]
		self.sampled_sound_headers=struct.unpack(">L", dFile.read(4))[0]       ##This bad boy is a poiner
        
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.subtitle_string_list_tag) != "None":
			tmpList.append((self.subtitle_string_list_tag, 'stli'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False
	def getTagName(self):
		return self.tagHeader.name