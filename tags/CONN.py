import tag_header,struct

class CONN:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        self.flags = struct.unpack(">L", dFile.read(4))[0]
        self.collection_reference_tag = struct.unpack(">L", dFile.read(4))[0]
        the_rest = dFile.read(24)
        
        
    def getLinkedTags(self):
        result=[]
        if tag_header.tagCheck(self.collection_reference_tag) != "None":
            result.append((self.collection_reference_tag, 'core'))
        return result