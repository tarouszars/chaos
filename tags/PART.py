import tag_header,struct

class PART:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        self.flags = struct.unpack(">L", dFile.read(4))[0]
    
        self.collection_reference_tag = struct.unpack(">L", dFile.read(4))[0]

        self.minimum_view_distance = struct.unpack(">l", dFile.read(4))[0]
        self.transparency_rolloff_point = struct.unpack(">l", dFile.read(4))[0]
        self.transparency_cutoff_point = struct.unpack(">l", dFile.read(4))[0]

        self.sequence_index = struct.unpack(">h", dFile.read(2))[0]

        self.number_of_particles = struct.unpack(">h", dFile.read(2))[0]
        self.maximum_particle_number_delta = struct.unpack(">h", dFile.read(2))[0]

        self.scale = struct.unpack(">h", dFile.read(2))[0]

        self.velocity_lower_bound = dFile.read(12) #really a struct fixed_vector3d
        self.velocity_delta = dFile.read(12) #really a struct fixed_vector3d

        self.x0_particle_number = struct.unpack(">h", dFile.read(2))[0]
        self.x1_particle_number = struct.unpack(">h", dFile.read(2))[0]
        self.y0_particle_number = struct.unpack(">h", dFile.read(2))[0]
        self.y1_particle_number = struct.unpack(">h", dFile.read(2))[0]

        self.state_variables = dFile.read(24)

        self.ambient_sound_tag = struct.unpack(">L", dFile.read(4))[0]
        self.splash_local_projectile_group_tag = struct.unpack(">L", dFile.read(4))[0]    

        the_rest = dFile.read(36)
        
    def getLinkedTags(self):
        result = []
        result.append((self.collection_reference_tag, "core"))
        result.append((self.ambient_sound_tag, "amso"))
        result.append((self.splash_local_projectile_group_tag, "lpgr"))
        return result