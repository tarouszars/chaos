import tag_header,struct

class MODE:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        self.flags = struct.unpack(">L", dFile.read(4))[0]
    
        self.geometry_tag = struct.unpack(">L", dFile.read(4))[0]
        rest_of_tag = dFile.read(56)
        
    def getLinkedTags(self):
        return [(self.geometry_tag, "geom")]