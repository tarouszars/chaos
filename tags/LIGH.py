import tag_header,struct
from tags.PROJ import damage_definition

class LIGH:
	def __init__(self,dFile,dOffset,localFile, filePath):
		self.fPath=filePath
		self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
		self.MAXIMUM_NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES=3
		if localFile>0:
			self.isLocal=True
		else:
			dFile.seek(self.tagHeader.getOffset())
			self.isLocal=False
		
		self.flags=struct.unpack(">L", dFile.read(4))[0]
		self.collection_reference_tag=struct.unpack(">L", dFile.read(4))[0]
		
		self.shock_duration=struct.unpack(">l", dFile.read(4))[0]
		self.fade_duration=struct.unpack(">l", dFile.read(4))[0]
		
		self.bolt_length=struct.unpack(">l", dFile.read(4))[0]
		self.sequence_indexes=[]
		for i in range(self.MAXIMUM_NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES):
			self.sequence_indexes.append(struct.unpack(">h", dFile.read(2))[0])
		
		self.velocity=struct.unpack(">h", dFile.read(2))[0]
		self.scale=struct.unpack(">h", dFile.read(2))[0]
		
		self.fork_segment_minimum=struct.unpack(">h", dFile.read(2))[0]
		self.fork_segment_delta=struct.unpack(">h", dFile.read(2))[0]
		
		self.fork_overlap_amount=struct.unpack(">h", dFile.read(2))[0] 
		self.main_bolt_overlap_amount=struct.unpack(">h", dFile.read(2))[0]
		
		self.angular_limit=struct.unpack(">H", dFile.read(2))[0]
		
		self.damage_definition=damage_definition(dFile);
		
		self.apparent_number_of_bolts=struct.unpack(">h", dFile.read(2))[0]
		
		collection_reference_index=struct.unpack(">h", dFile.read(2))[0]
		collection_index=struct.unpack(">h", dFile.read(2))[0]
		color_table_index=struct.unpack(">h", dFile.read(2))[0]
		
	def getLinkedTags(self):
		tmpList=[]
		if tag_header.tagCheck(self.collection_reference_tag) != "None":
			tmpList.append((self.collection_reference_tag, 'core'))
		if len(tmpList)>0:
			return tmpList
		else:
			return False