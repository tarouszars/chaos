import tag_header,struct

class AMSO:
    MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS = 6
    SIZEOF_STRUCT_AMBIENT_SOUND_DEFINITION = 64
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        #define 
        self.flags = struct.unpack(">L", dFile.read(4))[0]
        self.inner_radius = struct.unpack(">h", dFile.read(2))[0]
        self.outer_radius = struct.unpack(">h", dFile.read(2))[0]
        self.period_lower_bound = struct.unpack(">h", dFile.read(2))[0] 
        self.period_delta = struct.unpack(">h", dFile.read(2))[0]
        self.sound_tags = []
        for i in range(self.MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS):
            self.sound_tags.append(struct.unpack(">L", dFile.read(4))[0])
        self.random_sound_radius = struct.unpack(">h", dFile.read(2))[0]
        self.unused=[]
        for i in range(5):
            self.unused.append(struct.unpack(">h", dFile.read(2))[0])
        self.sound_indexes=[]
        for i in range(self.MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS):
            self.sound_indexes.append(struct.unpack(">h", dFile.read(2))[0])
        self.phase = struct.unpack(">h", dFile.read(2))[0] 
        self.period = struct.unpack(">h", dFile.read(2))[0]
        
        
    def getLinkedTags(self):
        tmpList=[]
        for sound in self.sound_tags:
            if tag_header.tagCheck(sound) != "None":
                tmpList.append((sound, 'soun'))
        return tmpList
        