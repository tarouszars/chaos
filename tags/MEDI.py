import tag_header,struct

MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS = 16

class MEDI:
    def __init__(self,dFile,dOffset,localFile, filePath):
        self.fPath=filePath
        self.tagHeader=tag_header.Tag_Header(dFile,dOffset)
        if localFile>0:
            self.isLocal=True
        else:
            dFile.seek(self.tagHeader.getOffset())
            self.isLocal=False
        self.flags = struct.unpack(">L", dFile.read(4))[0]
    
        self.collection_reference_tag = struct.unpack(">L", dFile.read(4))[0]
	
        self.unused = dFile.read(16)
        self.projectile_group_tags = []
        for i in range(MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS):
            self.projectile_group_tags.append(struct.unpack(">L", dFile.read(4))[0])

        self.reflection_tint_fraction = struct.unpack(">h", dFile.read(2))[0]

        self.reflection_tint_color = dFile.read(8)
	
        self.surface_effect_density = struct.unpack(">h", dFile.read(2))[0]
        self.surface_effect_local_projectile_group_tag = struct.unpack(">L", dFile.read(4))[0]

        rest = dFile.read(152)
        
    def getLinkedTags(self):
        result = []
        result.append((self.collection_reference_tag, "core"))
        for proj_group in self.projectile_group_tags:
            result.append((proj_group, "prgr"))
        result.append((self.surface_effect_local_projectile_group_tag, "lpgr"))
        return result